using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.IO;


namespace DataWedge
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class frmMain : System.Windows.Forms.Form
    {
        ////Test
        public const byte VK_LSHIFT = 0xA0; // left shift key
        public const byte VK_TAB = 0x09;
        public const byte VK_ENTER = 0x0D;
        public const byte VK_DOWN = 0x28;
        public const int KEYEVENTF_EXTENDEDKEY = 0x01;
        private System.Windows.Forms.Timer timer1;
        public const int KEYEVENTF_KEYUP = 0x02;

        [DllImport("user32.dll")]
        static extern void keybd_event(byte bVk, byte bScan, uint dwFlags, int dwExtraInfo);



        ////
               



    
        private string mPrefix = "";
        private string mSubfix = "";
        private string mEnter = "";
        private string mTab = "";
        private string mWinspeed = "";
        private IContainer components;
		private int listenport = 5555;
        private TcpListener listener;
		private ArrayList clients;
		private Thread processor;
		private Socket clientsocket;
        private NotifyIcon notifyIcon1;
        private ContextMenuStrip contextMenuStrip1;
        private ToolStripMenuItem toolStripMenuItem1;
        private ToolStripMenuItem toolStripMenuItem2;
        private ToolStripMenuItem exitToolStripMenuItem;
        private ListBox lbClients;
		private Thread clientservice;
        private ToolStripSeparator toolStripMenuItem3;
        private Client c;
		public frmMain()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			clients = new ArrayList();
			processor = new Thread(new ThreadStart(StartListening));
			processor.Start();
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripSeparator();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lbClients = new System.Windows.Forms.ListBox();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.contextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // notifyIcon1
            // 
            this.notifyIcon1.ContextMenuStrip = this.contextMenuStrip1;
            this.notifyIcon1.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon1.Icon")));
            this.notifyIcon1.Text = "DataWedge Server";
            this.notifyIcon1.Visible = true;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1,
            this.toolStripMenuItem2,
            this.toolStripMenuItem3,
            this.exitToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(153, 98);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(152, 22);
            this.toolStripMenuItem1.Text = "Settings";
            this.toolStripMenuItem1.Click += new System.EventHandler(this.toolStripMenuItem1_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(152, 22);
            this.toolStripMenuItem2.Text = "About";
            this.toolStripMenuItem2.Visible = false;
            this.toolStripMenuItem2.Click += new System.EventHandler(this.toolStripMenuItem2_Click);
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(149, 6);
            this.toolStripMenuItem3.Click += new System.EventHandler(this.toolStripMenuItem3_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // lbClients
            // 
            this.lbClients.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbClients.ItemHeight = 16;
            this.lbClients.Location = new System.Drawing.Point(12, 85);
            this.lbClients.Name = "lbClients";
            this.lbClients.Size = new System.Drawing.Size(264, 84);
            this.lbClients.TabIndex = 0;
            // 
            // frmMain
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(292, 181);
            this.Controls.Add(this.lbClients);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "DataWedge";
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.Shown += new System.EventHandler(this.ChatServer_Shown);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmMain_FormClosed);
            this.Resize += new System.EventHandler(this.ChatServer_Resize);
            this.contextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);

		}
		#endregion
        protected override void OnClosed(EventArgs e)
        {
          
        }
        private void closeProgram()
        {
            try
            {
                for (int n = 0; n < clients.Count; n++)
                {
                   
                        Client cl = (Client)clients[n];
                        SendToClient(cl, "QUIT|" + cl.Name);
                        Thread.Sleep(5000);
                        //cl.Sock.Close();
                        cl.CLThread.Abort();
                        clients.Remove(cl);
                   
              
                }
               listener.Stop();
               if (processor != null)
                   processor.Abort();
            }
            catch (Exception ex)
            {
                //Console.WriteLine(ex.ToString());
            }
        }
		private void StartListening()
		{
			listener = new TcpListener(listenport);
			listener.Start();
			while (true) {
				try
				{
					Socket s = listener.AcceptSocket();
					clientsocket = s;
					clientservice = new Thread(new ThreadStart(ServiceClient));
					clientservice.Start();
				}
				catch(Exception e)
				{
					Console.WriteLine(e.ToString() );
				}
			}
			//listener.Stop();
		}

        private void ServiceClient()
        {
            try
            {
                Socket client = clientsocket;
                bool keepalive = true;
                getbase();
                while (keepalive)
                {
                    //string[] c="{A,B,C}";
                    Byte[] buffer = new Byte[4096];
                    client.Receive(buffer);
                    string clientcommand = System.Text.Encoding.ASCII.GetString(buffer);

                    string[] tokens = clientcommand.Split(new Char[] { '|' });
                    //Console.WriteLine(clientcommand);

                    if (tokens[0] == "CONN")
                    {
                        /*for(int n=0; n<clients.Count; n++) {
                            Client cl = (Client)clients[n];
                            SendToClient(cl, "JOIN|" + tokens[1]);
                        }
                         */
                        EndPoint ep = client.RemoteEndPoint;
                        //string add = ep.ToString();
                        c = new Client(tokens[1], ep, clientservice, client);
                        clients.Add(c);
                        string message = "LIST|" + GetChatterList() + "\r\n";
                        SendToClient(c, message);

                        //lbClients.Items.Add(c.Name + " : " + c.Host.ToString());
                        //lbClients.Items.Add(c);
                        
                    }
                    if (tokens[0] == "CHAT")
                    {
                        string x = tokens[1];
                        getbase();
                      
                        for (int i = 0; i < tokens[1].Length; i++)
                        {

                            string a = Convert.ToString(x[i]) + Convert.ToString(x[i + 1]) + Convert.ToString(x[i + 2]);
                            if (x[i] == '\r')
                            {
                                KeybordEnTer.SendKeys(mSubfix, false);
                             
                                if (mEnter == "True")
                                {
                                        SimulateKey.Units.SimulateKey.SendEnter();
                                }
                                if (mTab == "True") SimulateKey.Units.SimulateKey.SendTab();
                                if (x[i + 1] == '\n') break;
                                else continue;
                            }
                            else if (a == "/G/")
                            {
                                i += 2;
                                KeybordEnTer.SendKeys(mPrefix, false);
                                continue;
                            }
                            else
                            {
                                KeybordEnTer.SendKeys(x[i].ToString(),false);
                                Thread.Sleep(30);
                            
                            }
                        }
                      

                    }
                    if (tokens[0] == "PRIV")
                    {
                        string destclient = tokens[3];
                        for (int n = 0; n < clients.Count; n++)
                        {
                            Client cl = (Client)clients[n];
                            if (cl.Name.CompareTo(tokens[3]) == 0)
                                SendToClient(cl, clientcommand);
                            if (cl.Name.CompareTo(tokens[1]) == 0)
                                SendToClient(cl, clientcommand);
                        }
                    }
                    if (tokens[0] == "GONE")
                    {
                        int remove = 0;
                        bool found = false;

                        for (int n = 0; n < clients.Count; n++)
                        {
                            Client cl = (Client)clients[n];
                            SendToClient(cl, clientcommand);
                            if (cl.Name.CompareTo(tokens[1]) == 0)
                            {
                                remove = n;
                                found = true;
                                //lbClients.Items.Remove(cl);
                                //lbClients.Items.Remove(cl.Name + " : " + cl.Host.ToString());
                            }
                        }
                        if (found)
                            clients.RemoveAt(remove);
                        client.Close();
                        keepalive = false;
                    }
                }
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
            
            }

        }

		private void SendToClient(Client cl, string message)
		{
			try{
				byte[] buffer = System.Text.Encoding.ASCII.GetBytes(message.ToCharArray());
				cl.Sock.Send(buffer,buffer.Length,0);
			}
			catch(Exception e){
				cl.Sock.Close();
				cl.CLThread.Abort();
				clients.Remove(cl);
			}
		}
		private string GetChatterList()
		{
			string chatters = "";
			for(int n=0; n<clients.Count; n++)
			{
				Client cl = (Client)clients[n];
				chatters += cl.Name;
				chatters += "|";
			}
			chatters.Trim(new char[]{'|'});
			return chatters;
		}
		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new frmMain());
		}

        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        public static extern IntPtr FindWindow(string classname, string windowname);
        [DllImport("user32.dll")]
        public static extern bool SetForegroundWindow(IntPtr p);

        private void ChatServer_Resize(object sender, EventArgs e)
        {
            if (FormWindowState.Minimized == WindowState)
                Hide();
        }

        private void ChatServer_Shown(object sender, EventArgs e)
        {
            Hide();
        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            frmSettings settings = new frmSettings();
            if (settings.ShowDialog() == DialogResult.OK)
            {

            }
            //Show();
            //WindowState = FormWindowState.Normal;
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
           
            closeProgram();
            Close();
            Application.Exit();
     
          
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            ////Test
            //System.Diagnostics.Process process = new System.Diagnostics.Process();
            //process.StartInfo.UseShellExecute = false;
            //process.StartInfo.RedirectStandardOutput = true;
            //process.StartInfo.RedirectStandardError = true;
            //process.StartInfo.CreateNoWindow = true;
            //process.StartInfo.FileName = "D:\\windows\\system32\\osk.exe";
            //process.StartInfo.Arguments = "";
            //process.StartInfo.WorkingDirectory = "D:\\";
            //process.Start(); // Start Onscreen Keyboard
            //process.WaitForInputIdle();
            //SetWindowPos(process.MainWindowHandle,
            //this.Handle, // Parent Window
            //this.Left, // Keypad Position X
            //this.Top + 20, // Keypad Position Y
            //panelButtons.Width, // Keypad Width
            //panelButtons.Height, // Keypad Height
            //SWP_SHOWWINDOW | SWP_NOZORDER); // Show Window and Place on Top
            //SetForegroundWindow1(process.MainWindowHandle);
            ////
        }
        private void getbase()
        {
             string fileName = "config.ini";
            string path = Application.StartupPath + "//" + fileName;
            int rec = 0;
            string line = "";
            StreamReader sr = new StreamReader(path);
            do
            {
                if (rec == 0)
                {
                    line = sr.ReadLine();
                    mPrefix = line.Remove(0, 8);
                }
                if (rec == 1)
                {
                    line = sr.ReadLine();
                    mSubfix = line.Remove(0, 8);
                }
                if (rec == 2)
                {
                    line = sr.ReadLine();
                    mEnter = line.Remove(0, 7);
                }
                if (rec == 3)
                {
                    line = sr.ReadLine();
                    mTab = line.Remove(0, 5);
                    break;
                }
              
                rec += 1;
                sr.Peek();
            } while (rec<=3);
            sr.Close();
            sr.Dispose();
        }

        private void frmMain_FormClosed(object sender, FormClosedEventArgs e)
        {
            //try
            //{
            //    Socket client = clientsocket;
            //    EndPoint ep = client.RemoteEndPoint;
            //    //string add = ep.ToString();
            //    //Client c = new Client(tokens[1], ep, clientservice, client);
            //    clients.Add(c);
            //    string message = "QUIT" + GetChatterList() + "\r\n";
            //    SendToClient(c, message);
            //}
            //catch (Exception ex)
            //{
            //    Console.WriteLine(ex.ToString());
            //}
            //base.OnClosed(e);
        }

        private void toolStripMenuItem3_Click(object sender, EventArgs e)
        {
            //string message = "QUIT|" + GetChatterList() + "\r\n";
            //SendToClient(c, message);
        
             //base.OnClosed(e);
        }

        private void toolStripMenuItem2_Click(object sender, EventArgs e)
        {
            frmTest frm = new frmTest();
          frm.ShowDialog();
         frm.Dispose();
        }

     
    }
}
