﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Text;
using System.Runtime.InteropServices;

public static class KeybordEnTer
{

private const uint INPUT_KEYBOARD = 1;
private const int KEY_EXTENDED = 0x0001;
private const uint KEY_UP = 0x0002;
//  private const uint KEY_SCANCODE = 0x0004;  //unicode 0x0008 Key_
private const uint KEYEVENTF_UNICODE =    0x0004; //THIS WORKS ASDEFAULT FLAG
private const uint KEYEVENTF_SCANCODE = 0x0008;   //IF I USE THIS IN THE FLAGS STRANGE RESULTS

[DllImport("User32.dll")]
static extern uint SendInput(uint nInputs, KEYBOARD_INPUT1[] pInputs, int cbSize);
//[MarshalAs(UnmanagedType.LPArray, SizeConst = 1)] KEYBOARD_INPUT[] input,

public struct KEYBDINPUT1
{
    public ushort wVk;
    public ushort wScan;
    public uint dwFlags;
    public long time;
    public uint dwExtraInfo;
}

[StructLayout(LayoutKind.Explicit, Size = 28)]
public struct KEYBOARD_INPUT1
{
    [FieldOffset(0)]
    public uint type;
    [FieldOffset(4)]
    public KEYBDINPUT1 ki;
}

public static void SendKeyP(char cKey)
{
ushort iKey = (byte)cKey;
SendKey(iKey,true);
}
/// <summary>
/// Presses a Key
/// </summary>
/// 
public static void SendKeyP(int scanCode)
{
SendKey(scanCode, true);
}

public static void SendKeyR(char cKey)
{
ushort iKey = (byte)cKey;
SendKey(iKey,false);
}
/// <summary>
/// Releases Key
/// </summary>
/// 
///
public static void SendKeyR(int scanCode)
{
SendKey(scanCode, false);
}
/// <summary>
/// single Char Key, Press and Release
/// </summary>
/// 
/// 
public static void SendKey(char cKey)
{
int iKey = (Byte)cKey;
SendKey(iKey, true);
SendKey(iKey, false);

}
/// <summary>
/// Single Int KeyCode hex or ascii
/// </summary>
/// 
/// 
public static void SendKey(int scanCode, bool press)
{
KEYBOARD_INPUT1[] input = new KEYBOARD_INPUT1[1];
input[0] = new KEYBOARD_INPUT1();
input[0].type = INPUT_KEYBOARD;
input[0].ki.dwFlags = KEYEVENTF_UNICODE;

if ((scanCode & 0xFF00) == 0xE000)
{ // extended key?
    input[0].ki.dwFlags |= KEY_EXTENDED;
}

if (press)
{ // press?
input[0].ki.wScan = (ushort)(scanCode & 0xFF);
}
else
{ // release?
    input[0].ki.wScan = (ushort)scanCode;
    input[0].ki.dwFlags |= KEY_UP;
    //input[0].ki.wVk = 13;
}
    //Edit
    //const int KEYEVENTF_EXTENDEDKEY = 0x1;
    //const int KEYEVENTF_KEYUP = 0x2;
    //input[0].ki.wVk = 0x0D;
    //input[0].ki.wScan = 0x45;
    //input[0].ki.dwFlags = KEYEVENTF_EXTENDEDKEY | KEYEVENTF_KEYUP;
    //input[0].ki.dwExtraInfo = 0;

        ///////////
uint result = SendInput(1, input, Marshal.SizeOf(input[0]));

if (result != 1)
{
throw new Exception("Could not send key: " + scanCode);
}
}


/// <summary>
/// String of 1 to many letters, words, etc example SendKeys("HelloWorld");
/// this does a Press and Release
/// </summary>
/// 
public static void SendKeys(string sKeys,bool Winspeed)
{
Byte[] aKeys = UTF8Encoding.ASCII.GetBytes(sKeys);
int iLen = aKeys.Length;
ushort scanCode = 0;
uint result;
for (int x = 0; x < iLen; x++)
{
//2 elements for each key, one for press one for Release
scanCode = aKeys[x];
KEYBOARD_INPUT1[] input = new KEYBOARD_INPUT1[2];
input[0] = new KEYBOARD_INPUT1();
input[0].type = INPUT_KEYBOARD;
if (Winspeed==true)
input[0].ki.wVk = (ushort)(scanCode & 0xFF);///Edit
input[0].ki.dwFlags = KEYEVENTF_UNICODE;

input[1] = new KEYBOARD_INPUT1();
input[1].type = INPUT_KEYBOARD;
if (Winspeed==true)
input[1].ki.wVk = (ushort)(scanCode & 0xFF);///Edit
input[1].ki.dwFlags = KEYEVENTF_UNICODE;
if ((scanCode & 0xFF00) == 0xE000)
{ // extended key?
    input[0].ki.dwFlags |= KEY_EXTENDED;
    input[1].ki.dwFlags |= KEY_EXTENDED;
}

//input[0] for key press
input[0].ki.wScan = (ushort)(scanCode & 0xFF);
//input[1] for KeyReleasse
input[1].ki.wScan =  (ushort)scanCode;
input[1].ki.dwFlags |= KEY_UP;
//call sendinput once for both keys
result = SendInput(2, input, Marshal.SizeOf(input[0]));
}
}

}
