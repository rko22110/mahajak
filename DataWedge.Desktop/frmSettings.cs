﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace DataWedge
{
     
    public partial class frmSettings : Form
    {
        private string mPrefix = "";
        private string mSubfix = "";
        private string mEnter = "";
        private string mTab = "";
        private string mWinspeed = "";
        //public static string NewLine { get; }

        public frmSettings()
        {
            InitializeComponent();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.No;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            
            string fileName = "config.ini";
            string path = Application.StartupPath + "//" + fileName;
            //StringBuilder classBudlder = new StringBuilder();
            bool chk  = System.IO.File.Exists(path);
           
       if (chk == true)
       {
           System.IO.File.Delete(path);
       
       }
    StreamWriter writer = System.IO.File.CreateText(path);
       writer.WriteLine("<Prefix>" + txtPrefix.Text);
       writer.WriteLine("<Subfix>" + txtSuffix.Text);
       //classBudlder.Append("<Prefix>"+txtPrefix.Text+"\n");
      // classBudlder.Append("<Subfix>" + txtSuffix.Text + "\n");
        if (chkEnter.Checked==true)
        {
            writer.WriteLine("<Enter>True");
            //classBudlder.Append("<Enter>True" + "\n");
        }
        else
        {
            //classBudlder.Append("<Enter>False" + "\n");
            writer.WriteLine("<Enter>False");
        }
            if (chkTAB.Checked==true)
            {
               // classBudlder.Append("<Tab>True" + "\n");
                writer.WriteLine("<Tab>True");
            }
            else
            {
               // classBudlder.Append("<Tab>False" + "\n");
                writer.WriteLine("<Tab>False");
            }
         

             //string text = classBudlder;
             //writer.Write(classBudlder);
            writer.Close();
            DialogResult = DialogResult.OK;
            
        }

        private void frmSettings_Load(object sender, EventArgs e)
        {
            getbase();
            txtPrefix.Text = mPrefix;
            txtSuffix.Text = mSubfix;
            if (mEnter == "True")
                chkEnter.Checked = true;
            else
                chkEnter.Checked = false;
            if (mTab == "True")
                chkTAB.Checked = true;
            else
                chkTAB.Checked = false;
      

        }
        private void getbase()
        {
            string fileName = "config.ini";
            string path = Application.StartupPath + "//" + fileName;
            int rec = 0;
            string line = "";
            StreamReader sr = new StreamReader(path);
            do
            {
                if (rec == 0)
                {
                    line = sr.ReadLine();
                    mPrefix = line.Remove(0, 8);
                }
                if (rec == 1)
                {
                    line = sr.ReadLine();
                    mSubfix = line.Remove(0, 8);
                }
                if (rec == 2)
                {
                    line = sr.ReadLine();
                    mEnter = line.Remove(0, 7);
                }
                if (rec == 3)
                {
                    line = sr.ReadLine();
                    mTab = line.Remove(0, 5);
                    break;
                }
            
                rec += 1;
                sr.Peek();
            } while (rec <= 3);
            sr.Close();
            sr.Dispose();

        }

        private void chkEnter_CheckedChanged(object sender, EventArgs e)
        {
          
        }
        //private string NewLineChars()
        //{
        //    throw new NotImplementedException();
        //}
   

        //private string Chr(int p)
        //{
        //    throw new NotImplementedException();
        //}

        //private string vbChr(int p)
        //{
        //    throw new NotImplementedException();
        //}

    }
}
