﻿namespace SmartDeviceProject1
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.MainMenu mainMenu1;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.button21 = new OpenNETCF.Windows.Forms.Button2();
            this.label1 = new System.Windows.Forms.Label();
            this.linkLabel21 = new OpenNETCF.Windows.Forms.LinkLabel2();
            this.batteryLife1 = new OpenNETCF.Windows.Forms.BatteryLife();
            this.batteryMonitor1 = new OpenNETCF.Windows.Forms.BatteryMonitor(this.components);
            this.textBox21 = new OpenNETCF.Windows.Forms.TextBox2();
            this.button1 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // button21
            // 
            this.button21.ForeColor = System.Drawing.SystemColors.ControlText;
            this.button21.Location = new System.Drawing.Point(37, 23);
            this.button21.Name = "button21";
            this.button21.Size = new System.Drawing.Size(72, 32);
            this.button21.TabIndex = 0;
            this.button21.Text = "button21";
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(48, 113);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 20);
            this.label1.Text = "label1";
            // 
            // linkLabel21
            // 
            this.linkLabel21.BackColor = System.Drawing.SystemColors.Control;
            this.linkLabel21.Location = new System.Drawing.Point(38, 188);
            this.linkLabel21.Name = "linkLabel21";
            this.linkLabel21.Size = new System.Drawing.Size(72, 20);
            this.linkLabel21.TabIndex = 2;
            this.linkLabel21.Text = "12345";
            // 
            // batteryLife1
            // 
            this.batteryLife1.Location = new System.Drawing.Point(37, 61);
            this.batteryLife1.Name = "batteryLife1";
            this.batteryLife1.Size = new System.Drawing.Size(152, 20);
            this.batteryLife1.TabIndex = 3;
            // 
            // textBox21
            // 
            this.textBox21.BackColor = System.Drawing.SystemColors.Window;
            this.textBox21.CharacterCasing = OpenNETCF.Windows.Forms.CharacterCasing.Normal;
            this.textBox21.ForeColor = System.Drawing.SystemColors.WindowText;
            this.textBox21.Location = new System.Drawing.Point(48, 232);
            this.textBox21.Name = "textBox21";
            this.textBox21.Size = new System.Drawing.Size(100, 22);
            this.textBox21.TabIndex = 4;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(38, 90);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(72, 20);
            this.button1.TabIndex = 5;
            this.button1.Text = "button1";
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(90, 125);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(100, 20);
            this.label2.Text = "label2";
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 268);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.textBox21);
            this.Controls.Add(this.batteryLife1);
            this.Controls.Add(this.linkLabel21);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button21);
            this.Menu = this.mainMenu1;
            this.Name = "Form2";
            this.Text = "Form2";
            this.ResumeLayout(false);

        }

        #endregion

        private OpenNETCF.Windows.Forms.Button2 button21;
        private System.Windows.Forms.Label label1;
        private OpenNETCF.Windows.Forms.LinkLabel2 linkLabel21;
        private OpenNETCF.Windows.Forms.BatteryLife batteryLife1;
        private OpenNETCF.Windows.Forms.BatteryMonitor batteryMonitor1;
        private OpenNETCF.Windows.Forms.TextBox2 textBox21;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label2;
    }
}