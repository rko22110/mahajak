﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data.SqlServerCe;
using System.IO;
using System.Data;
using System.Windows.Forms;
//using System.Configuration;

    public class SqlData
    {
        public static string strSQLCEConn = "Data Source=" + System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase)+ @"\DBMahajak.sdf";

        public static bool CommandSqlCEData(string command)
        {
            try
            {
                //if (!File.Exists(strSQLCEConn)) return false;
                using (SqlCeConnection cn = new SqlCeConnection(strSQLCEConn))
                {
                    cn.Open();
                    using (SqlCeCommand sqlcm = new SqlCeCommand(command, cn))
                    {
                        sqlcm.ExecuteNonQuery();
                    }
                    cn.Close();
                }
                return true;
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message, "MsgBox", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                return false;
            }
        }

        public static DataTable SelectSqlCEData(string command)
        {
            try
            {
                DataTable result = new DataTable();
                //if (!File.Exists(strSQLCEConn)) return null;
                using (SqlCeConnection cn = new SqlCeConnection(strSQLCEConn))
                {
                    cn.Open();
                    using (SqlCeDataAdapter sqlda = new SqlCeDataAdapter(command, cn))
                    {
                        sqlda.Fill(result);
                    }
                }
                return result;
            }
            catch { return null; }
        }
    }
