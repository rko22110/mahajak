﻿using System;
using System.Text;
using System.Runtime.InteropServices;
using System.Drawing;
using System.Windows.Forms;

namespace SocketClientPDA
{
    public class setAutoSize
    {
        private struct RECT
        {
            public int Left;

            public int Top;

            public int Right;

            public int Bottom;
        }

        private const int DT_CALCRECT = 0x400;

        private const int DT_CENTER = 0x1;

        private const int DT_LEFT = 0x0;

        private const int DT_RIGHT = 0x2;

        private const int DT_TOP = 0x0;

        private const int DT_WORDBREAK = 0x10;

        [System.Runtime.InteropServices.DllImport("coredll.dll")]
        private static extern int DeleteObject(IntPtr hObject);

        [System.Runtime.InteropServices.DllImport("coredll.dll")]
        private static extern int DrawText(IntPtr hdc, string lpStr, int nCount, ref RECT lpRect, int wFormat);

        [System.Runtime.InteropServices.DllImport("coredll.dll")]
        private static extern IntPtr SelectObject(IntPtr hdc, IntPtr hObject);

        public static void AutoSizeLabelHeight(Label ctlLabel)
    {

        // Auto size the height of a Label control based on the contents of the label.

        // Note: This routine is best called from the Form's Resize event so that changes to screen size and orientation

        // will force the Label height to be adjusted.

        

        try
        {
            RECT uRECT = new RECT();
            Graphics objGraphics = ctlLabel.TopLevelControl.CreateGraphics();

            IntPtr hDc = objGraphics.GetHdc();

            {
                var withBlock = ctlLabel;
                IntPtr hFont = withBlock.Font.ToHfont();
                IntPtr hFontOld = SelectObject(hDc, hFont);
                uRECT.Right = withBlock.Width;
                uRECT.Bottom = withBlock.Height;

                int lFormat = DT_CALCRECT | DT_WORDBREAK | DT_TOP;

                switch (withBlock.TextAlign)
                {
                    case ContentAlignment.TopLeft:
                        {
                            lFormat = lFormat | DT_LEFT;
                            break;
                        }

                    case ContentAlignment.TopCenter:
                        {
                            lFormat = lFormat | DT_CENTER;
                            break;
                        }

                    case ContentAlignment.TopRight:
                        {
                            lFormat = lFormat | DT_RIGHT;
                            break;
                        }
                }

                // -------------------------------------------------------------

                // Calculate the Rect of the text
                var rrr = uRECT;
                if (DrawText(hDc, withBlock.Text, -1, ref rrr, lFormat) != 0)
                {
                    withBlock.Height = uRECT.Bottom;
                }
                SelectObject(hDc, hFontOld);
                DeleteObject(hFont);
            }

            objGraphics.Dispose();
        }
        catch
        {
        }
        }
    }
}
