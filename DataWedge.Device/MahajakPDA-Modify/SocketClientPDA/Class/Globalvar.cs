﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Text;

    public class Globalvar
    {
        public static int getSuffixByTemplate(int templateId)
        {
            int suffix = 0;
            DataTable tblSuffix = SqlData.SelectSqlCEData(string.Format("select Suffix from Templates where TemplateID = {0}", templateId));
            if (tblSuffix.Rows.Count > 0)
            {
                suffix = Convert.ToInt32(tblSuffix.Rows[0]["Suffix"].ToString());
            }
            return suffix;
        }

        public static int getPrefixByTemplate(int templateId)
        {
            int prefix = 0;
            DataTable tblPrefix = SqlData.SelectSqlCEData(string.Format("select Prefix from Templates where TemplateID = {0}", templateId));
            if (tblPrefix.Rows.Count > 0)
            {
                prefix = Convert.ToInt32(tblPrefix.Rows[0]["Prefix"].ToString());
            }
            return prefix;
        }
    }

