﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.IO;

namespace SocketClient
{
    class Pallets
    {
        
        private DataSet _dataset = null;
        //sealed class mdGenarel
        //{
        //    public static string test = "";
        //}
	
        public Pallets()
        {
            _dataset = new DataSet();
            // create table
            DataTable dt = new DataTable();
            dt.TableName = "Barcode";
            dt.Columns.Add("Barcode", Type.GetType("System.String"));
            DataColumn[] keys = new DataColumn[1];
            keys[0] = dt.Columns["Barcode"];
            dt.PrimaryKey = keys; 
            _dataset.Tables.Add(dt);

        }

        public DataTable Table 
        { get { return _dataset.Tables[0]; } }

        public int Count
        { get { return _dataset.Tables[0].Rows.Count; } }

        public int Add(string value)
        {
                object[] objKeys = new object[1];
                objKeys[0] = value;
                DataRow row = _dataset.Tables[0].Rows.Find(objKeys);
                if (row == null)
                {
                    row = _dataset.Tables[0].NewRow();
                    row["Barcode"] = value;
                    _dataset.Tables[0].Rows.Add(row);
                    return 0; // normal
                }
                else
                {
                    return 1; // exist data
                }
        }

        public int RemoveAt(int Index)
        {
            try
            {
                _dataset.Tables[0].Rows.RemoveAt(Index);
                return 0;
            }
            catch
            {
                return 1;
            }
        }

        public bool SearchTextFile(string FileName, string key, int start, int length)
        {
            try
            {
                bool Found = false;
                using (StreamReader reader = new StreamReader(FileName))
                {
                    string line;
                    while ((line = reader.ReadLine()) != null)
                    {
                        if (line.Substring(0, length) == key)
                        {
                            Found = true;
                            break;
                        }
                    }
                }
                return Found;
            }
            catch
            {
                return false; // not exist
            }

        }

        public int Export2Txt(string FileName, string Header)
        {
            try
            {
                StreamWriter writer = new StreamWriter(FileName, true);
                if (Header != "") writer.WriteLine("HEADER,"+Header);
                foreach (DataRow row in _dataset.Tables[0].Rows)
                {
                    writer.WriteLine(row["Barcode"].ToString());                    
                }
                writer.Close();
                return 0; // normal
            }
            catch
            {
                return 1; // error
            }
        }

        public int DeleteAll()
        {
            DataRow row;
            while (_dataset.Tables[0].Rows.Count > 0)
            {
                row = _dataset.Tables[0].Rows[0];
                row.Delete();
            }

            return 0;
        }





    }
}
