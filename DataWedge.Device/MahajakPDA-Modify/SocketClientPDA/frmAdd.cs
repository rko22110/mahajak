﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using SocketClientPDA;
using Microsoft.Samples.Barcode;


namespace SocketClient
{
    public partial class frmAdd : Form
    {
        BarcodeScanner _scanner = null;

        public static int temPrefix, temSuffix, templateId;
        public static string barcode, templateName;
        DataTable tblTemplate;
        string barcodePrefix, barcodeSuffix;

        public frmAdd()
        {
            InitializeComponent();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
        }

        private void clearData()
        {
            //this.txtBarcode.Text = "";
            //if (cboRuning.Items.Count > 1)
            //        cboRuning.Items.Clear();
            //for (int i = 1 ; i<=100;i++)
            //{
                
            //    cboRuning.Items.Add(i);
            //}
            //cboRuning.SelectedIndex = 0;
        
        }

        private void frmAdd_Load(object sender, EventArgs e)
        {
            //clearData();
            LoadTemplate();

            _scanner = BarcodeScannerSingleton.Instance();
            _scanner.BarcodeScan += new BarcodeScanner.BarcodeScanEventHandler(scanner_BarcodeScan);
        }

        void scanner_BarcodeScan(object sender, BarcodeScannerEventArgs e)
        {
            barcode = e.Data.Replace("\r\n", "");
            //txtBarcode.Text = e.Data.Replace("\r\n", "");
            //txtBarcode.SelectionStart = txtBarcode.Text.Length;
            templateId = Convert.ToInt32(cboTemplate.SelectedValue.ToString());
            temPrefix = Globalvar.getPrefixByTemplate(templateId);
            temSuffix = Globalvar.getSuffixByTemplate(templateId);

            if (templateId > 1)
            {
                if (cboTemplate.Text == "AIR")
                {
                    barcodePrefix = barcode.Substring(0, temPrefix);
                    barcodeSuffix = barcode.Substring(barcode.Length - 1, 1);
                    barcode = barcode.Substring(temPrefix, barcode.Length - (temPrefix + temSuffix));
                }

                else if (cboTemplate.Text == "AMX")
                {
                    barcodeSuffix = barcode.Substring(barcode.Length - 4, 4);
                    barcode = barcode.Substring(temPrefix, barcode.Length - temSuffix);
                }

                else if (cboTemplate.Text == "Control 4")
                {
                    barcodePrefix = barcode.Substring(0, temPrefix);
                    barcode = barcode.Substring(temPrefix, barcode.Length - temPrefix);
                }
            }

            else
            {
                barcode = e.Data.Replace("\r\n", "");
            }

            txtBarcode.Text = barcode;
        }

        public Boolean IsNumeric(string num)
        {
            try
            {
                int newNum = Int32.Parse(num);

                return true;
            }

            catch
            {
                return false;

            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            //if (this.txtBarcode.Text.Length < 3)
            //{
            //    MessageBox.Show("Length of Barcode must > 2");
            //    return;
            //}
            //if (IsNumeric(txtBarcode.Text.Substring(txtBarcode.Text.Length - 3, 3)) == false)
            //{
            //    MessageBox.Show("Format Barcode is wrong");
            //    return;
            //}
            //Barcode = txtBarcode.Text;
            //Running = cboRuning.Text;
            barcode = txtBarcode.Text;
            templateName = cboTemplate.Text;
            this.DialogResult = DialogResult.OK;
        }

        private void frmAdd_KeyDown(object sender, KeyEventArgs e)
        {
            if ((e.KeyCode == System.Windows.Forms.Keys.Up))
            {
                // Up
            }
            if ((e.KeyCode == System.Windows.Forms.Keys.Down))
            {
                // Down
            }
            if ((e.KeyCode == System.Windows.Forms.Keys.Left))
            {
                // Left
            }
            if ((e.KeyCode == System.Windows.Forms.Keys.Right))
            {
                // Right
            }
            if ((e.KeyCode == System.Windows.Forms.Keys.Enter))
            {
                // Enter
            }

        }

        private void frmAdd_Closing(object sender, CancelEventArgs e)
        {
            _scanner.BarcodeScan += new BarcodeScanner.BarcodeScanEventHandler(scanner_BarcodeScan);
        }

        private void LoadTemplate()
        {
            tblTemplate = SqlData.SelectSqlCEData("select TemplateID,TemplateName from Templates");

            if (tblTemplate.Rows.Count > 0)
            {
                cboTemplate.DataSource = tblTemplate;
                cboTemplate.ValueMember = "TemplateID";
                cboTemplate.DisplayMember = "TemplateName";
            }
        }

        private void cboTemplate_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtBarcode.Text = string.Empty;
            txtBarcode.Focus();
        }

    }
}