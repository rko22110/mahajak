﻿using System;

using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Microsoft.Samples.Barcode;
using SocketClientPDA;

namespace SocketClient
{
    public partial class frmEdit : Form
    {
        BarcodeScanner _scanner = null;

        public static string NewBarcode = "";
        public static int temPrefix, temSuffix, templateId;
        public static string barcode;
        DataTable tblTemplate;
        string barcodePrefix, barcodeSuffix;

        public frmEdit()
        {
            InitializeComponent();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
        }

        private void txtBarcode_KeyPress(object sender, KeyPressEventArgs e)
        {
            
        }

        private void frmEdit_Load(object sender, EventArgs e)
        {
            lblTemplateName.Text = SocketClient.frmMain.templateName;
            txtBarcode.Text = SocketClient.frmMain.OldBarcode;
            txtBarcode.SelectAll();

            _scanner = BarcodeScannerSingleton.Instance();
            _scanner.BarcodeScan += new BarcodeScanner.BarcodeScanEventHandler(scanner_BarcodeScan);
        }

        void scanner_BarcodeScan(object sender, BarcodeScannerEventArgs e)
        {
            //this.txtBarcode.Text = e.Data.Replace("\r\n", "");
            //txtBarcode.SelectionStart = txtBarcode.Text.Length;
            barcode = e.Data.Replace("\r\n", "");
            //txtBarcode.Text = e.Data.Replace("\r\n", "");
            //txtBarcode.SelectionStart = txtBarcode.Text.Length;

            DataTable tblTemplate = SqlData.SelectSqlCEData(string.Format("select TemplateId from Templates where TemplateName = '{0}'", lblTemplateName.Text));

            if (tblTemplate.Rows.Count > 0)
            {
                templateId = Convert.ToInt32(tblTemplate.Rows[0]["TemplateId"].ToString());
                temPrefix = Globalvar.getPrefixByTemplate(templateId);
                temSuffix = Globalvar.getSuffixByTemplate(templateId);
            }


            if (templateId > 1)
            {
                if (lblTemplateName.Text == "AIR")
                {
                    barcodePrefix = barcode.Substring(0, temPrefix);
                    barcodeSuffix = barcode.Substring(barcode.Length - 1, 1);
                    barcode = barcode.Substring(temPrefix, barcode.Length - (temPrefix + temSuffix));
                }

                else if (lblTemplateName.Text == "AMX")
                {
                    barcodeSuffix = barcode.Substring(barcode.Length - 4, 4);
                    barcode = barcode.Substring(temPrefix, barcode.Length - temSuffix);
                }

                else if (lblTemplateName.Text == "Control 4")
                {
                    barcodePrefix = barcode.Substring(0, temPrefix);
                    barcode = barcode.Substring(temPrefix, 7);
                }
            }

            else
            {
                barcode = e.Data.Replace("\r\n", "");
            }

            txtBarcode.Text = barcode;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            //if (this.txtBarcode.Text == "")
            //{
            //    MessageBox.Show("Barcode can't blank !");
            //    return;
            //}

            NewBarcode = txtBarcode.Text;
            this.DialogResult = DialogResult.OK;

        }

        private void frmEdit_Closing(object sender, CancelEventArgs e)
        {
            _scanner.BarcodeScan += new BarcodeScanner.BarcodeScanEventHandler(scanner_BarcodeScan);
        }

        private void cboTemplate_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtBarcode.Focus();
            txtBarcode.SelectAll();
        }
    }
}