﻿using System;
using SocketClientPDA.Enums;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace SocketClient
{
    public partial class frmManageTemplate : Form
    {
        DataMode _mode;
        int _templateId, temPrefix, temSuffix;
        string cmd;

        public frmManageTemplate()
        {
            InitializeComponent();
        }

        public frmManageTemplate(DataMode mode): this()
        {
            _mode = mode;
            switch (mode)
            {
                case DataMode.Add:
                    this.Text = "Add Template";
                    txtTemplateName.Focus();
                    txtTemplateName.SelectAll();
                    break;

                case DataMode.Edit:
                    this.Text = "Edit Template";
                    txtTemplateName.Focus();
                    txtTemplateName.SelectAll();
                    break;

                default:
                    break;

            }
        }

        public void SetData(int id, string templateName, int prefix, int suffix)
        {
            _templateId = id;
            txtTemplateName.Text = templateName;
            nmrPrefix.Value = prefix;
            nmrSuffix.Value = suffix;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (_mode == DataMode.Add)
            {
                if (string.IsNullOrEmpty(txtTemplateName.Text))
                {
                    MessageBox.Show("Template Name is Missing, Please key Template Name Data");
                    txtTemplateName.Focus();
                    txtTemplateName.SelectAll();
                    return;
                }

                else
                {
                    temPrefix = Convert.ToInt32(nmrPrefix.Value);
                    temSuffix = Convert.ToInt32(nmrSuffix.Value);
                    cmd = string.Format("insert into Templates(TemplateName,Prefix,Suffix) values ('{0}', {1}, {2})", txtTemplateName.Text, temPrefix, temSuffix);

                    if (SqlData.CommandSqlCEData(cmd))
                    {
                        //frmTemplate.Instance().Show();
                        //this.Hide();
                        this.DialogResult = DialogResult.OK;
                    }
                }
            }

            else if (_mode == DataMode.Edit)
            {
                if (string.IsNullOrEmpty(txtTemplateName.Text))
                {
                    MessageBox.Show("Template Name is Missing, Please key Template Name Data");
                    txtTemplateName.Focus();
                    txtTemplateName.SelectAll();
                    return;
                }

                else
                {
                    temPrefix = Convert.ToInt32(nmrPrefix.Value);
                    temSuffix = Convert.ToInt32(nmrSuffix.Value);
                    cmd = string.Format("update Templates set TemplateName = '{0}', Prefix = {1}, " +
                        "Suffix = {2} where TemplateID = {3}", txtTemplateName.Text, temPrefix, temSuffix, _templateId);

                    if (SqlData.CommandSqlCEData(cmd))
                    {
                        //frmTemplate.Instance().Show();
                        //this.Hide();
                        this.DialogResult = DialogResult.OK;
                    }
                }
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            //frmTemplate.Instance().Show();
            //this.Hide();
        }
    }
}