﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using SocketClientPDA.Enums;

namespace SocketClient
{
    public partial class frmTemplate : Form
    {
        DataTable tblTemplate;
        int templateId, prefix, suffix;
        string templateName;

        //public static frmTemplate Instance()
        //{
        //    return new frmTemplate();
        //}

        public frmTemplate()
        {
            InitializeComponent();
        }

        private void frmTemplate_Load(object sender, EventArgs e)
        {
            AddColumnList();
            LoadTemplate();
        }

        private void LoadTemplate()
        {
            listTemplate.Items.Clear();
            tblTemplate = SqlData.SelectSqlCEData("select * from Templates");

            if (tblTemplate.Rows.Count > 0)
            {
                foreach (DataRow row in tblTemplate.Rows)
                {
                    ListViewItem listItem = new ListViewItem(row["TemplateID"].ToString());
                    listItem.SubItems.Add(row["TemplateName"].ToString());
                    listItem.SubItems.Add(row["Prefix"].ToString());
                    listItem.SubItems.Add(row["Suffix"].ToString());
                    listTemplate.Items.Add(listItem);
                }
            }
        }

        private void AddColumnList()
        {
            ColumnHeader columnHeader1 = new ColumnHeader();
            columnHeader1.Text = "No";
            columnHeader1.TextAlign = HorizontalAlignment.Center;
            columnHeader1.Width = 80;
            
            ColumnHeader columnHeader2 = new ColumnHeader();
            columnHeader2.Text = "TemplateName";
            columnHeader2.TextAlign = HorizontalAlignment.Center;
            columnHeader2.Width = 200;

            ColumnHeader columnHeader3 = new ColumnHeader();
            columnHeader3.Text = "Prefix";
            columnHeader3.TextAlign = HorizontalAlignment.Center;
            columnHeader3.Width = 100;

            ColumnHeader columnHeader4 = new ColumnHeader();
            columnHeader4.Text = "Suffix";
            columnHeader4.TextAlign = HorizontalAlignment.Center;
            columnHeader4.Width = 100;

            listTemplate.Columns.Add(columnHeader1);
            listTemplate.Columns.Add(columnHeader2);
            listTemplate.Columns.Add(columnHeader3);
            listTemplate.Columns.Add(columnHeader4);
        }

        private void btnBack_Click(object sender, EventArgs e)
        {

        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (listTemplate.SelectedIndices.Count > 0)
            {
                templateId = Convert.ToInt32(listTemplate.FocusedItem.SubItems[0].Text);
                templateName = listTemplate.FocusedItem.SubItems[1].Text;
                prefix = Convert.ToInt32(listTemplate.FocusedItem.SubItems[2].Text);
                suffix = Convert.ToInt32(listTemplate.FocusedItem.SubItems[3].Text);

                using (frmManageTemplate frmTemplateEdit = new frmManageTemplate(DataMode.Edit))
                {
                    frmTemplateEdit.SetData(templateId, templateName, prefix, suffix);

                    if (frmTemplateEdit.ShowDialog() == DialogResult.OK)
                    {
                        LoadTemplate();
                    }
                }
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            using (frmManageTemplate frmTemplateAdd = new frmManageTemplate(DataMode.Add))
            {
                frmTemplateAdd.ShowDialog();
            }

            LoadTemplate();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (listTemplate.SelectedIndices.Count > 0)
            {
                templateId = Convert.ToInt32(listTemplate.FocusedItem.SubItems[0].Text);
                templateName = listTemplate.FocusedItem.SubItems[1].Text;
                prefix = Convert.ToInt32(listTemplate.FocusedItem.SubItems[2].Text);
                suffix = Convert.ToInt32(listTemplate.FocusedItem.SubItems[3].Text);

                if (MessageBox.Show("Do you want to delete template?", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                {
                    SqlData.CommandSqlCEData(String.Format("Delete from Templates where TemplateID = '{0}' ", templateId));
                    listTemplate.Items.RemoveAt(listTemplate.SelectedIndices[0]);
                }
            }
        }
    }
}