﻿using System;

using System.Collections.Generic;
using System.Text;
using Microsoft.Samples.Barcode;

namespace SocketClientPDA
{
    public class BarcodeScannerSingleton
    {
        static BarcodeScanner _barcodeScanner = null;

        public static BarcodeScanner Instance()
        {
            if (_barcodeScanner == null)
            {
                _barcodeScanner = BarcodeScannerFacade.GetBarcodeScanner();
            }

            return _barcodeScanner;
        }
    }
}
