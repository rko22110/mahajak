//------------------------------------------------------------------------------
/// <copyright from='1997' to='2005' company='Microsoft Corporation'>
///		Copyright (c) Microsoft Corporation. All Rights Reserved.
///
///   This source code is intended only as a supplement to Microsoft
///   Development Tools and/or on-line documentation.  See these other
///   materials for detailed information regarding Microsoft code samples.
/// </copyright>
//------------------------------------------------------------------------------
using System;
//using Pidion.DCC;
using Bluebird;
using Bluebird.Barcode;
using System.Text;
using Microsoft.WindowsCE.Forms;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace Microsoft.Samples.Barcode.BluebirdScanner
{
    /// <summary>
    /// This is the barcode scanner class for Symbol devices.
    /// </summary>
    public class BluebirdBarcodeScanner : BarcodeScanner
    {
        private Bluebird.Barcode.Barcode bluebirdBarcode = null;
        MsgHandler m_MsgHandler;
        /// <summary>
        /// Initiates the Symbol scanner.
        /// </summary>
        /// <returns>Whether initialization was successful</returns>
        /// 

        public override bool Initialize()
        {
            // If scanner is already present then fail initialize
            //if (bluebirdBarcode != null)
            //    return false;
            // Create new scanner, first available scanner will be used.
            bluebirdBarcode = new Bluebird.Barcode.Barcode();
            
            m_MsgHandler = new MsgHandler(this);
            Start();
            // Create scanner data
            //bluebirdBarcode. += new BacodeScanEventHandler(barcode2D1_BacodeScan); 
            return true;
        }

        //  /// <summary>
        //  /// Start a Symbol scan.
        //  /// </summary>
        public override void Start()
        {
            // If we have both a scanner and data
            if (true != bluebirdBarcode.Open(true))
            {
                //MessageBox.Show("Barcode Open Fail");
            }
            else
            {
                if (true != bluebirdBarcode.SetClientHandle(m_MsgHandler.Hwnd)) // Set window handle that receive to message
                {
                    //MessageBox.Show("Set Client Error");
                }
            }
            bluebirdBarcode.SetVirtualWedge(false);
        }

        //  /// <summary>
        //  /// Stop a Symbol scan.
        //  /// </summary>
        public override void Stop()
        {
            // If we have a scanner
            bluebirdBarcode.ReleaseClientHandle();
            bluebirdBarcode.Close();
        }

        //  /// <summary>
        //  /// Terminates the Symbol scanner.
        //  /// </summary>
        public override void Terminate()
        {
            // If we have a scanner
            bluebirdBarcode.ReleaseClientHandle();
            bluebirdBarcode.Close();
            //bluebirdBarcode = null;
        }

        //  /// <summary>
        //  /// Event that fires when a Symbol scanner has performed a scan.
        //  /// </summary>
        //private void barcode2D1_BacodeScan(object sender, ScanEventArgs e)
        //{
        //    //스캔한 바코드 데이터의 값을 리스트에 추가한다.
        //    if (e.ScanData != "")
        //    {
        //        //ListViewItem lvt = new ListViewItem(new string[] { e.ScanData, e.ScanType });
        //        //listView1.Items.Add(lvt);
        //        OnBarcodeScan(new BarcodeScannerEventArgs(e.ScanData));
        //    }
        //}

        public class MsgHandler : MessageWindow
        {
            public const int WM_USER = 0x0400;
            public const int WM_SCANTRIGGER = WM_USER + 702;

            private BluebirdBarcodeScanner m_myForm;

            public MsgHandler(BluebirdBarcodeScanner form)
            {
                m_myForm = form;
            }
            // It is receive window message and get barcode data
            protected override void WndProc(ref Message msg)
            {
                switch (msg.Msg)
                {
                    case WM_SCANTRIGGER:
                        this.m_myForm.GetData();
                        //OnBarcodeScan(new BarcodeScannerEventArgs(msg.Msg));
                        break;
                }
            }

        }
        [DllImport("CoreDll.DLL", EntryPoint = "PlaySound", SetLastError = true)]
        private extern static int WCE_playsounduse(string szSound, IntPtr hMod, int flags);

        public void GetData()
        {
            //txtData.Text = "";

            byte[] uBuf = new byte[4096];

            int nBufSize = 4096;
            int nReadSize = 4096;
            byte type = new byte();

            //If there is NULL in Barcode data, use BBBarcodeGetDecodeDataNTypeRaw() function
            /*if (0 == m_Barcode.GetDecodeDataNTypeRaw(uBuf, ref type, nBufSize, ref nReadSize))
            {
                txtData.Text = Encoding.UTF8.GetString(uBuf, 0, uBuf.Length).Replace("\0", "").Trim();
                txtType.Text = GetBarcodeType(type);
            }*/
            //txtData.Text = "";
            StringBuilder buf = new StringBuilder(1024);

            if (bluebirdBarcode.GetDecodeDataNType(buf, ref type, nBufSize, ref nReadSize))
            {
                WCE_playsounduse("\\ProgramStore\\data\\barcode.wav", IntPtr.Zero, 0x00020001); 
                OnBarcodeScan(new BarcodeScannerEventArgs(buf.ToString()));
                //txtData.Text = buf.ToString();
                //txtType.Text = GetBarcodeType(type);
            }

            //Update();
        }
        //}

        #region IDisposable Members
        public override void Dispose(bool disposing)
        {
            if (disposing)
            {
                // Code to clean up managed resources
                Terminate();
            }
            // Code to clean up unmanaged resources
        }
        #endregion
    }
}
