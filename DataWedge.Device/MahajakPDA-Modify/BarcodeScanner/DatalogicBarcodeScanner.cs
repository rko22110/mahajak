//------------------------------------------------------------------------------
/// <copyright from='1997' to='2005' company='Microsoft Corporation'>
///		Copyright (c) Microsoft Corporation. All Rights Reserved.
///
///   This source code is intended only as a supplement to Microsoft
///   Development Tools and/or on-line documentation.  See these other
///   materials for detailed information regarding Microsoft code samples.
/// </copyright>
//------------------------------------------------------------------------------
using System;
using datalogic.datacapture;

namespace Microsoft.Samples.Barcode.DatalogicScanner
{
	/// <summary>
	/// This is the barcode scanner class for Datalogic devices.
	/// </summary>
    public class DatalogicBarcodeScanner : BarcodeScanner 
	{
        private Laser datalogicReader = null;

		/// <summary>
		/// Initiates the Datalogic scanner.
		/// </summary>
		/// <returns>Whether initialization was successful</returns>
		public override bool Initialize() 
		{
			// If scanner is already present then fail initialize
            if (datalogicReader != null) 
				return false;

//			try
//			{
            datalogicReader = new Laser();
//			}
//			catch (BarcodeReaderException be)
//			{
//				//MessageBox.Show(be.Message);
//				return false;
//			}
//			catch (Exception ex)
//			{
//				//MessageBox.Show(@"Make sure the ITCScan.DLL is in the \Windows directory");
//				return false;
//			}

			// Create event handler delegate
            datalogicReader.GoodReadEvent += new ScannerEngine.LaserEventHandler(datalogicReader_GoodReadEvent);

			// Setup reader
            datalogicReader.ScannerEnabled = true;

			return true;
		}

		/// <summary>
		/// Start a Datalogic scan (not needed).
		/// </summary>
		public override void Start() 
		{
		}

		/// <summary>
		/// Stop a Datalogic scan (not needed).
		/// </summary>
		public override void Stop() 
		{
		}

		/// <summary>
		/// Terminates the Datalogic scanner.
		/// </summary>
		public override void Terminate()
		{
			// If we have a scanner
            if (datalogicReader != null) 
			{
				// Free it up
                datalogicReader.Dispose();

				// Indicate we no longer have one
                datalogicReader = null;
			}
		}

		/// <summary>
		/// Event that fires when a Datalogic scanner has performed a scan.
		/// </summary>
        private void datalogicReader_GoodReadEvent(ScannerEngine sender)
		{
			// Raise read event to caller (with data)
			OnBarcodeScan(new BarcodeScannerEventArgs(sender.BarcodeDataAsText));
		}

    #region IDisposable Members
    public override void Dispose(bool disposing)
    {
      if (disposing)
      {
        // Code to clean up managed resources
        Terminate();
      }
      // Code to clean up unmanaged resources
    }
    #endregion
  }
}
