//------------------------------------------------------------------------------
/// <copyright from='1997' to='2005' company='Microsoft Corporation'>
///		Copyright (c) Microsoft Corporation. All Rights Reserved.
///
///   This source code is intended only as a supplement to Microsoft
///   Development Tools and/or on-line documentation.  See these other
///   materials for detailed information regarding Microsoft code samples.
/// </copyright>
//------------------------------------------------------------------------------
using System;
using Intermec.DataCollection;

namespace Microsoft.Samples.Barcode.IntermecScanner
{
	/// <summary>
	/// This is the barcode scanner class for Intermec devices.
	/// </summary>
	public class IntermecBarcodeScanner : BarcodeScanner 
	{
        private BarcodeReader intermecReader = null;

		/// <summary>
		/// Initiates the Intermec scanner.
		/// </summary>
		/// <returns>Whether initialization was successful</returns>
		public override bool Initialize() 
		{
			// If scanner is already present then fail initialize
			if ( intermecReader != null ) 
				return false;

//			try
//			{
				intermecReader = new BarcodeReader();
//			}
//			catch (BarcodeReaderException be)
//			{
//				//MessageBox.Show(be.Message);
//				return false;
//			}
//			catch (Exception ex)
//			{
//				//MessageBox.Show(@"Make sure the ITCScan.DLL is in the \Windows directory");
//				return false;
//			}

			// Create event handler delegate
			intermecReader.BarcodeRead +=new BarcodeReadEventHandler(intermecReader_BarcodeRead);

			// Setup reader
			intermecReader.ThreadedRead(true);

			return true;
		}

		/// <summary>
		/// Start a Intermec scan (not needed).
		/// </summary>
		public override void Start() 
		{
		}

		/// <summary>
		/// Stop a Intermec scan (not needed).
		/// </summary>
		public override void Stop() 
		{
		}

		/// <summary>
		/// Terminates the Intermec scanner.
		/// </summary>
		public override void Terminate()
		{
			// If we have a scanner
			if ( intermecReader != null ) 
			{
				// Free it up
				intermecReader.Dispose();

				// Indicate we no longer have one
				intermecReader = null;
			}
		}

		/// <summary>
		/// Event that fires when a Intermec scanner has performed a scan.
		/// </summary>
		private void intermecReader_BarcodeRead(object sender, BarcodeReadEventArgs bre)
		{
			// Raise read event to caller (with data)
			OnBarcodeScan(new BarcodeScannerEventArgs(bre.strDataBuffer));
		}

    #region IDisposable Members
    public override void Dispose(bool disposing)
    {
      if (disposing)
      {
        // Code to clean up managed resources
        Terminate();
      }
      // Code to clean up unmanaged resources
    }
    #endregion
  }
}
