﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Net.Sockets;
using System.IO;
using System.Threading;
using OpenNETCF.Configuration;
using Symbol.Fusion.WLAN;
using Symbol.Fusion;
using Symbol.Exceptions;
using OpenNETCF.Windows.Forms;
namespace SocketClient
{
    public partial class frmMain : Form
    {
        private bool isActive = false;
        public static string  OldBarcode = "";
        private bool chkOpenProgram = true;
        private Pallets pallets;
        private int serverport;
        private NetworkStream ns;
        private StreamReader sr;
        private TcpClient clientsocket;
        private Thread receive = null;
        private string serveraddress;
        private bool connected = false;
        private bool logging = false;
        private bool privatemode = false;
        private string id;
        private bool closeCon = false;
        private WLAN myWlan = null;
        //private ConfigSettings config = new ConfigSettings();
        private String IPAddress = "";
        private String Port = "";
        private Symbol.WirelessLAN.Radio MyRadio = null;
        private System.EventHandler MyEventHandler = null;
        private Symbol.Barcode.Reader MyReader = null;
        private Symbol.Barcode.ReaderData MyReaderData = null;
        private System.EventHandler ReaderFormEventHandler;
        private Resources MyResources = null;
        //Config object for Version and Diagnostics
        private Config myConfig = null;
        //WLAN object for WLAN operations
   
        //A temporary Adhoc profile that is created by this sample for demonstration
        private Profile myAdhocProfile = null;
        //A temporary profile that is created by this sample for demonstration
        private Profile myInfrastructureProfile = null;
        //The flag to track whether the profile SampleAdhoc is created by this sample or not. Initialized to false.
        private bool bAdhocCreated = false;
        //The flag to track whether the profile SampleInfrastructure is created by this sample or not. Initialized to false.
        private bool bInfraCreated = false;
        Adapter.SignalQualityHandler mySignalQualityHandler = null;
        //the event handler for status changes
  
        private static bool bPortrait = true;   // The default dispaly orientation 
        // has been set to Portrait.

        private bool bSkipMaxLen = false;    // The restriction on the maximum 
        // physical length is considered by default.

        private bool bInitialScale = true;   // The flag to track whether the 
        // scaling logic is applied for
        // the first time (from scatch) or not.
        // Based on that, the (outer) width/height values
        // of the form will be set or not.
        // Initially set to true.

        private int resWidthReference = 249;   // The (cached) width of the form. 
        // INITIALLY HAS TO BE SET TO THE WIDTH OF THE FORM AT DESIGN TIME (IN PIXELS).
        // This setting is also obtained from the platform only on
        // Windows CE devices before running the application on the device, as a verification.
        // For PocketPC (& Windows Mobile) devices, the failure to set this properly may result in the distortion of GUI/viewability.

        private int resHeightReference = 225;  // The (cached) height of the form.
        // INITIALLY HAS TO BE SET TO THE HEIGHT OF THE FORM AT DESIGN TIME (IN PIXELS).
        // This setting is also obtained from the platform only on
        // Windows CE devices before running the application on the device, as a verification.
        // For PocketPC (& Windows Mobile) devices, the failure to set this properly may result in the distortion of GUI/viewability.

        private const double maxLength = 5.5;  // The maximum physical width/height of the sample (in inches).
        // The actual value on the device may slightly deviate from this
        // since the calculations based on the (received) DPI & resolution values 
        // would provide only an approximation, so not 100% accurate.

        // Delegate used to show message boxes in the main thread.
        private delegate void UpdateUIDelegate(string Message, string Caption);
        // Handler invoked in the event notification to display message boxes in the main thread
        private UpdateUIDelegate updateUIHandler = null;
       
        // Ensures that ReadNotify handler is not attached multiple times.
        private bool IsReadNotificationAttached = false;
     
        public frmMain()
        {
            WLAN.Monitor.AdapterPower = true;

            Cursor savedCursor = Cursor.Current;
            Cursor.Current = Cursors.WaitCursor;

            MyResources = new Resources();
            InitializeComponent();
            myConfig = new Config(FusionAccessType.STATISTICS_MODE);
            //create a reference to WLAN
            myWlan = new WLAN(FusionAccessType.STATISTICS_MODE);

            createSampleProfiles();

            //refresh profile list so that newly created profiles will be visible
            myWlan.Profiles.Refresh();

            Cursor.Current = savedCursor;
           
        }
      
        private void getbase()
        {
            //string fileName = "./config.ini";
            //string path = Application.StartupPath + "//" + fileName;
            int rec = 0;
            string line = "";
            StreamReader sr = new StreamReader(Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase) + "\\config.ini");
            do
            {
                if (rec == 0)
                {
                    line = sr.ReadLine();
                    IPAddress = line.Remove(0, 11);
                }

                if (rec == 1)
                {
                    line = sr.ReadLine();
                    Port = line.Remove(0, 6);
                    break;
                }
                rec += 1;
                sr.Peek();
            } while (rec <= 3);
            sr.Close();

        }
        private void EstablishConnection()
        {
            //statusBar1.Text = "Connecting to Server";
            try
            {
                getbase();
                serveraddress = IPAddress;
                serverport = Convert.ToInt32(Port);
                clientsocket = new TcpClient(serveraddress, serverport);
                ns = clientsocket.GetStream();
                sr = new StreamReader(ns);
                connected = true;
            }
            catch (Exception e)
            {
               // MessageBox.Show(e.Message);//"Could not connect to Server");
               // statusBar1.Text = "Disconnected";
            }
        }

        private void RegisterWithServer()
        {
            try
            {
                Random random = new Random();
                id = random.Next(1, 100).ToString(); 
                string command = "CONN|"+ id;// +ChatOut.Text;
                Byte[] outbytes = System.Text.Encoding.ASCII.GetBytes(command.ToCharArray());
                ns.Write(outbytes, 0, outbytes.Length);

                string serverresponse = sr.ReadLine();
                serverresponse.Trim();
                string[] tokens = serverresponse.Split(new Char[] { '|' });
                if (tokens[0] == "LIST")
                {
                  //  statusBar1.Text = "Connected";
                    mnuDisconnect.Enabled = true;
                }
                //for (int n = 1; n < tokens.Length - 1; n++)
                //    lbChatters.Items.Add(tokens[n].Trim(new char[] { '\r', '\n' }));
                //this.Text = clientname + ": Connected to Chat Server";

            }
            catch (Exception e)
            {
                //MessageBox.Show("Error Registering");
            }
        }

        private void ReceiveChat()
        {
            bool keepalive = true;
            bool disCon = false;
            //Console.WriteLine("receivechat");
            while (keepalive)
            {
                try
               {
                    Byte[] buffer = new Byte[2048];
                    ns.Read(buffer, 0, buffer.Length);
                    string chatter = System.Text.Encoding.ASCII.GetString(buffer,0,2048);

                    string[] tokens = chatter.Split(new Char[] { '|' });
                    Console.WriteLine(chatter);

                    //if (tokens[0] == "CHAT")
                    //{
                    //    rtbChatIn.AppendText(tokens[1]);
                    //    if (logging)
                    //        logwriter.WriteLine(tokens[1]);
                    //}
                    /*if (tokens[0] == "PRIV")
                    {
                        rtbChatIn.AppendText("Private from ");
                        rtbChatIn.AppendText(tokens[1].Trim());
                        rtbChatIn.AppendText(tokens[2] + "\r\n");
                        if (logging)
                        {
                            logwriter.Write("Private from ");
                            logwriter.Write(tokens[1].Trim());
                            logwriter.WriteLine(tokens[2] + "\r\n");
                        }
                    }*/
                    /*if (tokens[0] == "JOIN")
                    {
                        rtbChatIn.AppendText(tokens[1].Trim());
                        rtbChatIn.AppendText(" has joined the Chat\r\n");
                        if (logging)
                        {
                            logwriter.WriteLine(tokens[1] + " has joined the Chat");
                        }
                        string newguy = tokens[1].Trim(new char[] { '\r', '\n' });
                        lbChatters.Items.Add(newguy);
                    }*/
                    /*if (tokens[0] == "GONE")
                    {
                        rtbChatIn.AppendText(tokens[1].Trim());
                        rtbChatIn.AppendText(" has left the Chat\r\n");
                        if (logging)
                        {
                            logwriter.WriteLine(tokens[1] + " has left the Chat");
                        }
                        lbChatters.Items.Remove(tokens[1].Trim(new char[] { '\r', '\n' }));
                    }*/
                    if (tokens[0] == "QUIT")
                    {
                        //ns.Close();
                        //clientsocket.Close();

                        keepalive = false;
                        //connected = false;
                        //btnSend.Enabled = false;
                        //showDisconnect = false;
                        //statusBar1.Text = "Server has stopped";
                        //disConFromServer();
                        //closeCon = true;
                        //timer1.Enabled = true;
                        //bool chk = false;
                        //while (chk == false)
                        //{
                        //    try
                        //    {

                        //        disConnect();
                        //        chk = true;
                        //    }
                        //    catch { 
                            
                        //    }
                        
                        //}
                        //disCon = true;
                        disConFromServer();
                     
                    }
                }
                catch (Exception e) {
                //    //MessageBox.Show(e.ToString());
                }
            }
            //mnuDisconnect.Enabled = showDisconnect;


         
        }
     

        private void QuitChat()
        {
            try
            {
                if (connected)
                {
                    try
                    {
                        string command = "GONE|" + id;// +clientname;
                        Byte[] outbytes = System.Text.Encoding.ASCII.GetBytes(command.ToCharArray());
                        ns.Write(outbytes, 0, outbytes.Length);
                        clientsocket.Close();
                    }
                    catch (Exception ex)
                    {
                    }
                }
                //if (logging)
                //    logwriter.Close();

                if (receive != null)// && receive.IsAlive)
                    receive.Abort();
                this.Text = "Client";
            }
            catch { 
            
            }
        }
        private Profile getProfileByName(string profileName, WLAN myNewWlan)
        {
            Symbol.Fusion.WLAN.Profiles myProfiles = myNewWlan.Profiles;
            //traverse all Profiles
            for (int profileIndex = 0; profileIndex < myProfiles.Length; profileIndex++)
            {
                Profile myProfile = myProfiles[profileIndex];
                if (profileName == myProfile.Name)
                {
                    return myProfile;
                }
            }
            return null;
        }
        private void createSampleProfiles()
        {
            //Create a WLAN object in COMMAND_MODE
            WLAN myCommandModeWlan = null;
            try
            {
                myCommandModeWlan = new WLAN(FusionAccessType.COMMAND_MODE);
            }
            catch (OperationFailureException)
            {
                System.Windows.Forms.MessageBox.Show("Command mode is in use", "CS_FusionSample1");
            
                return;
            }

            //if (!findProfile("SampleAdhoc"))
            if (getProfileByName("SampleAdhoc", myCommandModeWlan) == null)
            {
                //Create a simple adhoc profile
                AdhocProfileData myAdhocProfileData = new AdhocProfileData("SampleAdhoc", "SampleSSID");
                try
                {
                    myAdhocProfile = myCommandModeWlan.Profiles.CreateAdhocProfile(myAdhocProfileData);
                    bAdhocCreated = true;
                }
                catch
                {
                    // This is just to allow the application to continue even if the profile creation fails ...
                }

            }
            //if (!findProfile("SampleInfrastructure"))
            if (getProfileByName("SampleInfrastructure", myCommandModeWlan) == null)
            {
                //Create a simple infrastructure profile
                InfrastructureProfileData myInfrastructureProfileData = new InfrastructureProfileData("SampleInfrastructure", "CODE123");
                try
                {
                    myInfrastructureProfile = myCommandModeWlan.Profiles.CreateInfrastructureProfile(myInfrastructureProfileData);
                    bInfraCreated = true;
                }
                catch
                {
                    // This is just to allow the application to continue even if the profile creation fails ...
                }
            }

            //Dispose COMMAND_MODE WLAN object
            myCommandModeWlan.Dispose();
            myCommandModeWlan = null;

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            if (Scanning.InitReader())
            {
                // Create a new delegate to handle scan notifications
                ReaderFormEventHandler = new EventHandler(MyReader_ReadNotify);

                // Set the event handler of the Scanning class to our delegate
                Scanning.MyEventHandler = ReaderFormEventHandler;

                // Attach to activate and deactivate events
                this.Activated += new EventHandler(ReaderForm_Activated);

                // Start a read on the reader
                Scanning.StartRead();
            }
            else
            {
                // If not, close this form
                this.Close();

                return;
            }
            //this.MyReader = new Symbol.Barcode.Reader();
            //this.MyReader.Actions.Enable();

            chkOpen();
        }
        private void chkOpen()
        { 
          try
            {

                mySignalQualityHandler = new Adapter.SignalQualityHandler(myAdapter_SignalQualityChanged);

                // Add MainMenu if Pocket PC
                //if (Symbol.Win32.PlatformType.IndexOf("PocketPC") != -1)
                //{
                //    this.Menu = new MainMenu();
                //}
                Adapter myAdapter = myWlan.Adapters[0];
                displaySignal(myAdapter.SignalQuality.ToString());
                // Add SignalQualityChanged event handler
                myAdapter.SignalQualityChanged += mySignalQualityHandler;
                listView1.Visible = false;
                this.btnAdd.Visible = false;
                this.btnClear.Visible = false;
                this.btnEdit.Visible = false;
                this.btnGoBarCode.Visible = false;
                this.btnDelete.Visible = false;
                this.btnSend.Visible = false;
                this.pnShowConnect.Visible = true;
                this.mnuBrower.Enabled = false;
                chkOpenProgram = true;
            }
            catch {
                MessageBox.Show("No Wireless Signal. You can use only Print Mode.");
                deleteSampleProfiles();
                chkOpenProgram = false;
                disConnect();
                mnuMode2.Checked = !mnuMode2.Checked;
                mnuMode1.Checked = !mnuMode2.Checked;
                ShowMode();
                pnSignal.Visible = false;
            }
        
        }
        private bool InitReader()
        {
            // If reader is already present then fail initialize
            if (this.MyReader != null)
            {
                return false;
            }

            try
            {
                //if (Symbol.Barcode.Device.AvailableDevices.Length > 1)
                //{
                //    SelectDevForm devFrm = new SelectDevForm();
                //    devFrm.DoScale();
                //    devFrm.ShowDialog();
                //    string devName = devFrm.GetDeviceName();
                //    if ((devName != null) && (devName != ""))
                //    {
                //        this.MyReader = new Symbol.Barcode.Reader(new Symbol.Barcode.Device(devName));
                //    }
                //    else
                //    {
                //        MessageBox.Show("No device selected.");
                //        return false;
                //    }

                //}

                //else
                //{
                    // Create new reader, first available reader will be used.
                    this.MyReader = new Symbol.Barcode.Reader();
                //}

                // Create reader data
                this.MyReaderData = new Symbol.Barcode.ReaderData(
                    Symbol.Barcode.ReaderDataTypes.Text,
                    Symbol.Barcode.ReaderDataLengths.MaximumLabel);

                // Create event handler delegate
                this.MyEventHandler = new EventHandler(MyReader_ReadNotify);

                // Enable reader, with wait cursor
                this.MyReader.Actions.Enable();

                this.MyReader.Parameters.Feedback.Success.BeepTime = 0;
                this.MyReader.Parameters.Feedback.Success.WaveFile = "\\windows\\alarm3.wav";

                // Attach to activate and deactivate events
                this.Activated += new EventHandler(ReaderForm_Activated);
                this.Deactivate += new EventHandler(ReaderForm_Deactivate);
            }

            catch (Symbol.Exceptions.OperationFailureException ex)
            {
                MessageBox.Show("InitReader\n" +
                    "Operation Failure\n" + ex.Message +
                    "\n" +
                    "Result = " + (Symbol.Results)((uint)ex.Result)
                    );
            }
            catch (Symbol.Exceptions.InvalidRequestException ex)
            {
                MessageBox.Show("InitReader\n" +
                    "Invalid Request\n" +
                    ex.Message);
            }
            catch (Symbol.Exceptions.InvalidIndexerException ex)
            {
                MessageBox.Show("InitReader\n" +
                    "Invalid Indexer\n" +
                    ex.Message);
            };


            return true;
        }
        private void ReaderForm_Activated(object sender, EventArgs e)
        {
            // Reset the scan event handler for the Scanning object to the form's delegate. 
            Scanning.MyEventHandler = ReaderFormEventHandler;

            // Start the next read
            Scanning.StartRead();
            isActive = true;
        }

        /// <summary>
        ///  Handler for when the form is deactivated.  This would be called if another application became the current
        ///  application. This stops the reading of data from the reader.
        /// </summary>
        /// 
        private void StartRead()
        {
            // If we have both a reader and a reader data
            if ((this.MyReader != null) &&
                (this.MyReaderData != null))
            {
                try
                {
                    if (false == IsReadNotificationAttached)
                    {
                        this.MyReader.ReadNotify += this.MyEventHandler;
                        IsReadNotificationAttached = true;
                    }

                    // Submit a read
                    this.MyReader.Actions.Read(this.MyReaderData);
                }

                catch (Symbol.Exceptions.OperationFailureException ex)
                {
                    string sMsg = "StartRead\n" +
                    "Operation Failure\n" + ex.Message +
                    "\n" +
                    "Result = " + (Symbol.Results)((uint)ex.Result);

                    this.Invoke(this.updateUIHandler, new object[] { sMsg, string.Empty });

                    if ((Symbol.Results)(ex.Result) == Symbol.Results.E_SCN_READINCOMPATIBLE)
                    {
                        // If the failure is E_SCN_READINCOMPATIBLE, exit the application.
                        sMsg = "The application will now exit.";
                        this.Invoke(this.updateUIHandler, new object[] { sMsg, string.Empty });

                        this.Close();
                        return;
                    }
                }
                catch (Symbol.Exceptions.InvalidRequestException ex)
                {
                    string sMsg = "StartRead\n" +
                        "Invalid Request\n" +
                        ex.Message;

                    this.Invoke(this.updateUIHandler, new object[] { sMsg, string.Empty });
                }
                catch (Symbol.Exceptions.InvalidIndexerException ex)
                {
                    string sMsg = "StartRead\n" +
                        "Invalid Indexer\n" +
                        ex.Message;

                    this.Invoke(this.updateUIHandler, new object[] { sMsg, string.Empty });
                };
            }
        }

        /// <summary>
        /// Stop all reads on the reader
        /// </summary>
        private void StopRead()
        {
            // If we have a reader
            if (this.MyReader != null)
            {
                try
                {
                    if (true == IsReadNotificationAttached)
                    {
                        // Remove read notification handler
                        this.MyReader.ReadNotify -= this.MyEventHandler;
                        IsReadNotificationAttached = false;
                    }

                    // Flush (Cancel all pending reads)
                    this.MyReader.Actions.Flush();
                }

                catch (Symbol.Exceptions.OperationFailureException ex)
                {
                    MessageBox.Show("StopRead\n" +
                    "Operation Failure\n" + ex.Message +
                    "\n" +
                    "Result = " + (Symbol.Results)((uint)ex.Result)
                    );
                }
                catch (Symbol.Exceptions.InvalidRequestException ex)
                {
                    MessageBox.Show("StopRead\n" +
                        "Invalid Request\n" +
                        ex.Message);
                }
                catch (Symbol.Exceptions.InvalidIndexerException ex)
                {
                    MessageBox.Show("StopRead\n" +
                        "Invalid Indexer\n" +
                        ex.Message);
                };
            }
        }

        /// <summary>
        /// Read complete or failure notification
        /// </summary>
        private void ReaderForm_Deactivate(object sender, EventArgs e)
        {
            Scanning.StopRead();
            isActive = false;
        }
        private void buttonStopListen_Click(object sender, EventArgs e)
        {

        }
        private void btnDisconnect_Click(object sender, EventArgs e)
        {
            disConnect();
            mnuDisconnect.Enabled = false;
            mnuConnect.Enabled = true;
            mnuMenu.Enabled = true;
            lblBarcode.BackColor = Color.White;
            lblBarcode.ForeColor = Color.Black;
           // statusBar1.Text = "Disconnected";
            this.pnShowConnect.Visible = true;
            this.mnuBrower.Enabled = false;
            this.btnAdd.Visible = false;
            this.btnClear.Visible = false;
            this.btnEdit.Visible = false;
            this.btnSend.Visible = false;
            this.btnGoBarCode.Visible = false;
            this.btnDelete.Visible = false;
            this.listView1.Visible = false;

        }
        private void disConFromServer()
        {

            try
            {
                DoUpdateMnuDisCon(false);
                DoUpdateMnuCon(true);
                DoUpdateMnuMenu(true);
                DoUpdatelblBackCol(Color.White);
                DoUpdatelblForceCol(Color.Black);
                DoUpdateStatus("Disconnected");
                //QuitChat();
                //mnuDisconnect.Enabled = false;
                //mnuConnect.Enabled = true;
                ////btnSend.Enabled = false;
              
                ////lbChatters.Items.Clear();
                //mnuMenu.Enabled = true;
                //lblBarcode.BackColor = Color.White;
                //lblBarcode.ForeColor = Color.Black;
                //statusBar1.Text = "Disconnected";
               connected = false;
                ns.Close();
                clientsocket.Close();
                MessageBox.Show("Cannot connect to Server!");
               //this.pnShowConnect.Visible = true;
                DoUpdatePNShow(true);
               //this.btnSend.Visible = false;
                DoUpdateBtnSend(false);
               //this.btnGoBarCode.Visible = false;
                DoUpdateBtnGoBarCode(false);
               //this.btnDelete.Visible = false;
                DoUpdateBtnDelete(false);
               //this.listView1.Visible = false;
                DoUpdateLvw(false);
               receive.Abort();
            }
            catch (Exception e)
            {
                //MessageBox.Show(e.ToString());
            }
        }
        private void DoUpdateLvw(bool message)
        {
            if (this.InvokeRequired)
            {
                // we were called on a worker thread
                // marshal the call to the user interface thread
                this.Invoke(new UpdateStatusDelegate(DoUpdateLvw),
                            new object[] { message });
                return;
            }

            // this code can only be reached
            // by the user interface thread
            //this.textBox1.Text = message;
            listView1.Visible = message;
        }
        private void DoUpdateBtnDelete(bool message)
        {
            if (this.InvokeRequired)
            {
                // we were called on a worker thread
                // marshal the call to the user interface thread
                this.Invoke(new UpdateStatusDelegate(DoUpdateBtnDelete),
                            new object[] { message });
                return;
            }

            // this code can only be reached
            // by the user interface thread
            //this.textBox1.Text = message;
            btnDelete.Visible = message;
        }

        private void DoUpdateBtnGoBarCode(bool message)
        {
            if (this.InvokeRequired)
            {
                // we were called on a worker thread
                // marshal the call to the user interface thread
                this.Invoke(new UpdateStatusDelegate(DoUpdateBtnGoBarCode),
                            new object[] { message });
                return;
            }

            // this code can only be reached
            // by the user interface thread
            //this.textBox1.Text = message;
            btnGoBarCode.Visible = message;
        }

        private void DoUpdateBtnSend(bool message)
        {
            if (this.InvokeRequired)
            {
                // we were called on a worker thread
                // marshal the call to the user interface thread
                this.Invoke(new UpdateStatusDelegate(DoUpdateBtnSend),
                            new object[] { message });
                return;
            }

            // this code can only be reached
            // by the user interface thread
            //this.textBox1.Text = message;
            btnSend.Visible = message;
        }

        private void DoUpdatePNShow(bool message)
        {
            if (this.InvokeRequired)
            {
                // we were called on a worker thread
                // marshal the call to the user interface thread
                this.Invoke(new UpdateStatusDelegate(DoUpdatePNShow),
                            new object[] { message });
                return;
            }

            // this code can only be reached
            // by the user interface thread
            //this.textBox1.Text = message;
            pnShowConnect.Visible= message;
        }



        private void DoUpdateStatus(string message)
        {
            if (this.InvokeRequired)
            {
                // we were called on a worker thread
                // marshal the call to the user interface thread
                this.Invoke(new UpdateStatusDelegateString(DoUpdateStatus),
                            new object[] { message });
                return;
            }

            // this code can only be reached
            // by the user interface thread
            //this.textBox1.Text = message;
           // statusBar1.Text = message;

        }
        private void DoUpdatelblForceCol(Color message)
        {
            if (this.InvokeRequired)
            {
                // we were called on a worker thread
                // marshal the call to the user interface thread
                this.Invoke(new UpdateStatusDelegateColor(DoUpdatelblForceCol),
                            new object[] { message });
                return;
            }

            // this code can only be reached
            // by the user interface thread
            //this.textBox1.Text = message;
            lblBarcode.BackColor = message;

        }
        private void DoUpdatelblBackCol(Color message)
        {
            if (this.InvokeRequired)
            {
                // we were called on a worker thread
                // marshal the call to the user interface thread
                this.Invoke(new UpdateStatusDelegateColor(DoUpdatelblBackCol),
                            new object[] { message });
                return;
            }

            // this code can only be reached
            // by the user interface thread
            //this.textBox1.Text = message;
            lblBarcode.BackColor = message;
         
        }
        private void DoUpdateMnuMenu(bool message)
        {
            if (this.InvokeRequired)
            {
                // we were called on a worker thread
                // marshal the call to the user interface thread
                this.Invoke(new UpdateStatusDelegate(DoUpdateMnuMenu),
                            new object[] { message });
                return;
            }

            // this code can only be reached
            // by the user interface thread
            //this.textBox1.Text = message;
            mnuMenu.Enabled = message;
        }
        private void DoUpdateMnuCon(bool message)
        {
            if (this.InvokeRequired)
            {
                // we were called on a worker thread
                // marshal the call to the user interface thread
                this.Invoke(new UpdateStatusDelegate(DoUpdateMnuCon),
                            new object[] { message });
                return;
            }

            // this code can only be reached
            // by the user interface thread
            //this.textBox1.Text = message;
            mnuConnect.Enabled = message;
        }
        private void DoUpdateMnuDisCon(bool message)
        {
            if (this.InvokeRequired)
            {
                // we were called on a worker thread
                // marshal the call to the user interface thread
                this.Invoke(new UpdateStatusDelegate(DoUpdateMnuDisCon),
                            new object[] { message });
                return;
            }

            // this code can only be reached
            // by the user interface thread
            //this.textBox1.Text = message;
            mnuDisconnect.Enabled = message;
        }
        private delegate void UpdateStatusDelegate(bool message);
        private delegate void UpdateStatusDelegateColor(Color message);
        private delegate void UpdateStatusDelegateString(string message);
        private void disConnect()
        {
                //Thread.Sleep(100);

            if (connected == true)
            {
                QuitChat();
                mnuDisconnect.Enabled = false;
                mnuConnect.Enabled = true;
                mnuMenu.Enabled = true;
                lblBarcode.BackColor = Color.White;
                lblBarcode.ForeColor = Color.Black;
               // statusBar1.Text = "Disconnected";
                //btnSend.Enabled = false;
                ns.Close();
                clientsocket.Close();
                if (receive != null)
                    receive.Abort();
                connected = false;
                //lbChatters.Items.Clear();
                this.pnShowConnect.Visible = true;
                this.mnuBrower.Enabled = false;
                this.btnAdd.Visible = false;
                this.btnClear.Visible = false;
                this.btnEdit.Visible = false;
                this.btnSend.Visible = false;
                this.btnGoBarCode.Visible = false;
                this.btnDelete.Visible = false;
                this.listView1.Visible = false;
            }
           
        }
    
        private void disconnectOnly() {
            if (connected == true)
            {
                QuitChat();
            
                //btnSend.Enabled = false;
                ns.Close();
                clientsocket.Close();
                if (receive!=null)
                receive.Abort();
                connected = false;
                //lbChatters.Items.Clear();
            
           }
        
        }
        private void btnSend_Click(object sender, EventArgs e)
        {
            //if (Convert.ToInt32(this.LabelSignalPercent.Text.Replace("%","")) <= 10)
            //{

            //    MessageBox.Show("Cannot send data because no wireless signal. !!!");
            //    return;
            //}
            if (mnuMode1.Checked == true) { 
              EstablishConnection();
            if (connected == false) {
                MessageBox.Show("Cannot connect to server because your wireless or PC not ready. Please try again. !!!");
                return;
            }
            else
            {
                RegisterWithServer();
                receive = new Thread(new ThreadStart(ReceiveChat));
                receive.Start();
            
            }
            }
          
                if (this.listView1.Items.Count < 1)
                    return;
            if (mnuMode2.Checked)
            {
                try
                {
                    bool chkPrint = true;
                    if (mnu3Unit.Checked == true)
                    {
                       
                        int count = 0;
                        string bar1 = "";
                        string bar2 = "";
                        string bar3 = "";
                        for (int i = 0; i < listView1.Items.Count; i++)
                        {
                            count += 1;
                            if (count == 1) bar1 = listView1.Items[i].SubItems[0].Text;
                            if (count == 2) bar2 = listView1.Items[i].SubItems[0].Text;
                            if (count == 3)
                            {
                                bar3 = listView1.Items[i].SubItems[0].Text;
                                chkPrint = PrintBarcodeTriple(bar1, bar2, bar3);
                                if (chkPrint == false) return;
                                count = 0;
                                bar1 = "";
                                bar2 = "";
                                bar3 = "";
                            }
                            if ((listView1.Items.Count % 3) == 1)
                            {
                                if (i == (listView1.Items.Count - 1))
                                {
                                    chkPrint = PrintBarcodeTriple(bar1, "", "");
                                    if (chkPrint == false) return;
                                }
                            }
                            if ((listView1.Items.Count % 3) == 2)
                            {
                                if (i == (listView1.Items.Count - 1))
                                {
                                    chkPrint = PrintBarcodeTriple(bar1, bar2, "");
                                    if (chkPrint == false) return;
                                }
                            }

                        }



                    }
                    else if (mnu1Unit.Checked == true)
                    {
                        for (int i = 0; i < listView1.Items.Count; i++)
                        {


                            chkPrint = PrintBarcode(listView1.Items[i].SubItems[0].Text);
                            if (chkPrint == false) return;

                        }
                    }
                    else
                    {
                        int count = 0;
                        string bar1 = "";
                        string bar2 = "";
                        for (int i = 0; i < listView1.Items.Count; i++)
                        {
                            count += 1;
                            if (count == 1) bar1 = listView1.Items[i].SubItems[0].Text;

                            if (count == 2)
                            {
                                bar2 = listView1.Items[i].SubItems[0].Text;
                                PrintBarcodeDouble(bar1, bar2);
                                count = 0;
                                bar1 = "";
                                bar2 = "";
                            }
                            if ((listView1.Items.Count % 2) == 1)
                            {
                                if (i == (listView1.Items.Count - 1))
                                {
                                    PrintBarcodeDouble(bar1, "");

                                }
                            }

                        }
                        //2 barcode
                    
                    }
                    if (chkPrint == true)
                    {
                        //listView1.Items.Clear();
                        //lblCount.Text = Convert.ToString(0);
                        //btnDelete.Enabled = false;
                        //btnSend.Enabled = false;
                    }
                    return;
                }
                catch
                {
                }

            }
            else
            {
                try
                {          
                string pubcommand = "CHAT|";
                for (int i = 0; i < listView1.Items.Count; i++)
                {
                    pubcommand += "/G/" + listView1.Items[i].SubItems[0].Text + "\r";
                }
                pubcommand += "\n";
                string dclient = "";
                if (!privatemode)
                {
                   

                    Byte[] outbytes = System.Text.Encoding.ASCII.GetBytes(pubcommand.ToCharArray());
                    ns.Write(outbytes, 0, outbytes.Length);

                    lblBarcode.Text = "";
                    txtBarcode.Text = "";
                    txtBarcode.Focus();
                }
                //listView1.Items.Clear();
                //lblCount.Text = Convert.ToString(0);
                //btnDelete.Enabled = false;
                //btnSend.Enabled = false;
                MessageBox.Show("Send Data Complete");
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Connection with Server lost");
                    ns.Close();
                    clientsocket.Close();
                    if (receive != null)// && receive.IsAlive)
                        receive.Abort();
                    connected = false;
                    //statusBar1.Text = "Disconnected";
                }
            }
            disconnectOnly();
        }

        private void textMsg_KeyPress(object sender, KeyPressEventArgs e)
        {
           
            //if (Convert.ToChar(e.KeyChar) == '\r')
            //{
            //    //MessageBox.Show("Complete");
            //    e.Handled = true;
            //    if (mnuMode1.Checked || mnuMode2.Checked)
            //    {
            //        if (this.listView1.Visible == true)
            //        {
            //            //lblBarcode.Text = txtBarcode.Text;
            //            //string strBool = ConfigurationSettings.AppSettings["Confirm"];
            //            //btnSend.Visible = Convert.ToBoolean(strBool);
            //            //if (mnuMode1.Checked && connected)
            //            //{
            //            //    if (!btnSend.Visible) // auto
            //            //        btnSend_Click(sender, e);
            //            //}
            //            //else if (mnuMode2.Checked)
            //            //{
            //            //    if (!btnSend.Visible) // auto
            //            //        PrintBarcode();
            //            //}
            //            string barcode = txtBarcode.Text;
            //            //int i = barcode.IndexOf("\r\n");
            //            ////barcode = barcode.Substring(0, i);


            //            //if (pallets.Add(barcode) == 1)
            //            //    MessageBox.Show(string.Format("Pallet No. {0} exist data in this file.!", barcode));
            //            //else
            //            //{
            //            //    //lblQty.Text = string.Format("{0}", Convert.ToInt16(lblQty.Text) + 1);
            //            //    //EnableControl(true);
            //            //}
            //            if (this.listView1.Items.Count < 100)
            //            {
            //                ListViewItem lvw = new ListViewItem();
            //                lvw.SubItems[0].Text = barcode;
            //                listView1.Items.Add(lvw);
            //                lblCount.Text = Convert.ToString(Convert.ToInt32(lblCount.Text) + 1);
            //            }
            //            else
            //            {
            //                MessageBox.Show("Maximum Line is 100 Line");
            //            }
                      
            //            txtBarcode.Text = "";
            //            txtBarcode.Focus();
            //            btnDelete.Enabled = true;
            //            btnSend.Enabled = true;
            //        }

            //    }
            //}
        }
        private bool PrintBarcodeTriple(string pBarCode1, string pBarCode2,string pBarCode3)
        {
            serialPort1.PortName = ConfigurationSettings.AppSettings["COMPort"];
            string baud = ConfigurationSettings.AppSettings["Baudrate"];
            serialPort1.BaudRate = Convert.ToInt32(baud);
            try
            {
                serialPort1.Open();

                serialPort1.Write("! 0 200 200 80 1\r\n");
                //serialPort1.Write("LABEL\r\n");
                serialPort1.Write("CONTRAST 1\r\n");
                serialPort1.Write("TONE 100\r\n");
                serialPort1.Write("SPEED 5\r\n");
                serialPort1.Write("GAP-SENSE\r\n");
                serialPort1.Write("PAGE-WIDTH 800\r\n");
                serialPort1.Write(";// PAGE 0000000006400176\r\n");
                serialPort1.Write("BT 0 3 8\r\n");
                int x1 = Convert.ToInt32((264 - (((pBarCode1.Length * 3.5)) * 8)) / 2);
                int x2 = 264 + Convert.ToInt32((264 - (((pBarCode2.Length * 3.5) ) * 8)) / 2);
                int x3 = 528 + Convert.ToInt32((264 - (((pBarCode3.Length * 3.5) ) * 8)) / 2);
                if (x1 < 9) serialPort1.Write("B 128 1 0 30 9 10 " + pBarCode1 + "\r\n");
                else serialPort1.Write("B 128 1 0 30 " + Convert.ToString(x1) + " 10 " + pBarCode1 + "\r\n");

                if (pBarCode2 != "")
                {
                    if (x2 < 274)
                        serialPort1.Write("B 128 1 0 30 274 10 " + pBarCode2 + "\r\n");
                    else
                        serialPort1.Write("B 128 1 0 30 " + Convert.ToString(x2) + " 10 " + pBarCode2 + "\r\n");

                }
                if (pBarCode3 != "")
                {
                    if (x3 < 538)
                        serialPort1.Write("B 128 1 0 30 538 10 " + pBarCode3 + "\r\n");
                    else
                        serialPort1.Write("B 128 1 0 30 " + Convert.ToString(x3) + " 10 " + pBarCode3 + "\r\n");

                }


                serialPort1.Write("FORM\r\n");
                serialPort1.Write("PRINT\r\n");
                serialPort1.WriteTimeout = 500000000;//แก้ตรงนี้
                //serialPort1.Write("\r\n");


                //MessageBox.Show("Print Success");
                Thread.Sleep(100);
                serialPort1.Close();
                lblBarcode.Text = "";
                txtBarcode.Text = "";
                txtBarcode.Focus();
                return true;
            }
            catch (Exception e)
            {
                serialPort1.Close();
                MessageBox.Show("Can't open port");
                return false;
            }
        }
        private void PrintBarcodeDouble(string pBarCode1,string pBarCode2)
        {
            serialPort1.PortName = ConfigurationSettings.AppSettings["COMPort"];
            string baud = ConfigurationSettings.AppSettings["Baudrate"];
            serialPort1.BaudRate = Convert.ToInt32(baud);
            try
            {
                serialPort1.Open();

                serialPort1.Write("! 0 200 200 160 1\r\n");
                //serialPort1.Write("LABEL\r\n");
                serialPort1.Write("CONTRAST 1\r\n");
                serialPort1.Write("TONE 100\r\n");
                serialPort1.Write("SPEED 5\r\n");
                serialPort1.Write("GAP-SENSE\r\n");
                serialPort1.Write("PAGE-WIDTH 800\r\n");
                serialPort1.Write(";// PAGE 0000000006400176\r\n");
                serialPort1.Write("BT 0 3 8\r\n");
                int x1 = Convert.ToInt32((400 - ((pBarCode1.Length * 3.5) * 8)) / 2);
                int x2 = 400 + Convert.ToInt32((400 - ((pBarCode2.Length * 3.5) * 8)) / 2);

                if (x1<9) serialPort1.Write("B 128 1 0 30 9 10 " + pBarCode1+"\r\n" );
                else serialPort1.Write("B 128 1 0 30 "+Convert.ToString(x1) +" 10 " + pBarCode1+"\r\n" );

                if (pBarCode2 != "")
                {
                    if (x2 < 410) 
                    serialPort1.Write("B 128 1 0 30 410 10 " + pBarCode2 + "\r\n");
                    else
                    serialPort1.Write("B 128 1 0 30 " + Convert.ToString(x2) + " 10 " + pBarCode2 + "\r\n");
                   
                }
                serialPort1.Write("FORM\r\n");
                serialPort1.Write("PRINT\r\n");
                serialPort1.WriteTimeout = 500000000;
                //serialPort1.Write("\r\n");


                //MessageBox.Show("Print Success");
                Thread.Sleep(100);
                serialPort1.Close();

                lblBarcode.Text = "";
                txtBarcode.Text = "";
                txtBarcode.Focus();
            }
            catch (Exception e)
            {
                //MessageBox.Show(e.Message);
                serialPort1.Close();
                MessageBox.Show("Can't open port");
            }
        }

        private bool PrintBarcode(string pBarCode)
        {
            serialPort1.PortName =ConfigurationSettings.AppSettings["COMPort"];
            string baud = ConfigurationSettings.AppSettings["Baudrate"];
            serialPort1.BaudRate = Convert.ToInt32( baud );
            try
            {
                serialPort1.Open();
               
                serialPort1.Write("! 0 200 200 160 1\r\n");
                //serialPort1.Write("LABEL\r\n");
                serialPort1.Write("CONTRAST 1\r\n");
                serialPort1.Write("TONE 100\r\n");
                serialPort1.Write("SPEED 5\r\n");
                serialPort1.Write("GAP-SENSE\r\n");
                serialPort1.Write("PAGE-WIDTH 800\r\n");
                serialPort1.Write(";// PAGE 0000000006400176\r\n");
                serialPort1.Write("BT 0 3 8\r\n");
  //              int x1 = Convert.ToInt32((800 - (((pBarCode.Length * 2.5) + 2) * 8)) / 2); //Convert.ToInt32((640 - (pBarCode.Length / 0.727) * 15) / 2);
                int x1 = Convert.ToInt32((800 - ((pBarCode.Length *3.5) * 8) )/ 2); //Convert.ToInt32((640 - (pBarCode.Length / 0.727) * 15) / 2);
                if (pBarCode.Length >= 13) x1 = Convert.ToInt32((800 - (((pBarCode.Length * 3.5)-4) * 8)) / 2);

                if (x1 < 9) serialPort1.Write("B 128 1 0 30 9 10 " + pBarCode + "\r\n");
                else serialPort1.Write("B 128 1 0 30 " + Convert.ToString(x1) + " 10 " + pBarCode + "\r\n");
                serialPort1.Write("FORM\r\n");
                serialPort1.Write("PRINT\r\n");
                serialPort1.WriteTimeout = 500000000; //แก้ตรงนี้
                //serialPort1.Write("\r\n");


                //MessageBox.Show("Print Success");
                Thread.Sleep(100);
                serialPort1.Close();

                lblBarcode.Text = "";
                txtBarcode.Text = "";
                txtBarcode.Focus();
                return true;            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
                serialPort1.Close();
                MessageBox.Show("Can't open port");
                return false;
            }

        }

        private void btnConnect_Click(object sender, EventArgs e)
        {
            //if (ChatOut.Text == "")
            //{
            //    MessageBox.Show("Enter a name in the box before connecting", "Error",
            //        MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            //    return;
            // }
            // else
            //     clientname = ChatOut.Text;

            EstablishConnection();

            if (connected)
            {
                //RegisterWithServer();
                //receive = new Thread(new ThreadStart(ReceiveChat));
                //receive.Start();
                disconnectOnly();
                //btnSend.Enabled = true;
                mnuConnect.Enabled = false;
                mnuDisconnect.Enabled = true;
                txtBarcode.Text = "";
                txtBarcode.Focus();

                lblBarcode.BackColor = Color.Orange;
                lblBarcode.ForeColor = Color.White;

                mnuMenu.Enabled = false;
                this.pnShowConnect.Visible = false;
                this.mnuBrower.Enabled = true;
                this.btnSend.Visible = true;
                this.btnAdd.Visible = true;
                this.btnClear.Visible = true;
                this.btnEdit.Visible = true;
                this.btnGoBarCode.Visible = true;
                this.btnDelete.Visible = true;
                this.listView1.Visible = true;
            }
            else {
                MessageBox.Show("Cannot connect to server because your wireless or PC not ready. Please try again. !!!");
                return;
            
            
            }
        }

        private void frmMain_Activated(object sender, EventArgs e)
        {
           txtBarcode.Focus();
           Scanning.TermReader();
           if (Scanning.InitReader())
           {
               // Create a new delegate to handle scan notifications
               ReaderFormEventHandler = new EventHandler(MyReader_ReadNotify);

               // Set the event handler of the Scanning class to our delegate
               Scanning.MyEventHandler = ReaderFormEventHandler;

               // Attach to activate and deactivate events
               this.Activated += new EventHandler(ReaderForm_Activated);

               // Start a read on the reader
               Scanning.StartRead();
           }
           else
           {
               // If not, close this form
               this.Close();

               return;
           }
        }

        private void frmMain_Closed(object sender, EventArgs e)
        {
            if (connected)
                btnDisconnect_Click(sender, e);
        }

        private void menuItem3_Click(object sender, EventArgs e)
        {
            if (connected ==true) disConnect();
            Close();
        }

        private void mnuSettings_Click(object sender, EventArgs e)
        {
            using (frmSettings settings = new frmSettings())
            {
                if (settings.ShowDialog() == DialogResult.OK)
                {
                }
            }
        }

        private void frmMain_GotFocus(object sender, EventArgs e)
        {
            txtBarcode.Focus();
        }

        //private void button1_Click(object sender, EventArgs e)
        //{
        //    PrintBarcode();
        //}

        private void mnuMode1_Click(object sender, EventArgs e)
        {
            if (chkOpenProgram == false){



                chkOpen();
            }
            mnuMode1.Checked = !mnuMode1.Checked;
            mnuMode2.Checked = !mnuMode1.Checked;
            ShowMode();
        }

        private void mnuMode2_Click(object sender, EventArgs e)
        {
            disConnect();
            mnuMode2.Checked = !mnuMode2.Checked;
            mnuMode1.Checked = !mnuMode2.Checked;
            ShowMode();
            pnSignal.Visible = false;
        }

        private void ShowMode()
        {

            if (mnuMode1.Checked)
            {
                lblMode.Text = "Mode: DataWedge";
                mnuConnect.Enabled = true;
                btnSend.Text = "Send Data";
            this.pnShowConnect.Visible = true;
            this.mnuBrower.Enabled = false;
            this.btnSend.Visible = false;
            this.btnAdd.Visible = false;
            this.btnClear.Visible = false;
            this.btnEdit.Visible = false;
            this.btnGoBarCode.Visible = false;
            this.btnDelete.Visible = false;
            this.listView1.Visible = false;
            }
            else
            {
                lblMode.Text = "Mode: Print Barcode";
                mnuConnect.Enabled = false;
                btnSend.Text = "Print Data";
            this.pnShowConnect.Visible = false;
            this.mnuBrower.Enabled = true;
            this.btnSend.Visible = true;
            this.btnAdd.Visible = true;
            this.btnClear.Visible = true;
            this.btnEdit.Visible = true;
            this.btnGoBarCode.Visible = true;
            this.btnDelete.Visible = true;
            this.listView1.Visible = true;
            }
          
            

        }

        private void button1_Click_1(object sender, EventArgs e)
        {
        }

        private void btnGoBarCode_Click(object sender, EventArgs e)
        {
            txtBarcode.Focus();
            btnGoBarCode.Enabled = false;
            btnGoBarCode.BackColor = Color.Gainsboro;
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (listView1.SelectedIndices.Count > 0)
            {

                if (MessageBox.Show("Do you want to delete barcode?", "Warning",MessageBoxButtons.YesNo, MessageBoxIcon.Question,MessageBoxDefaultButton.Button1) == DialogResult.Yes) 

                {
                  
                    listView1.Items.RemoveAt(listView1.FocusedItem.Index );
                    lblCount.Text = Convert.ToString(Convert.ToInt32(lblCount.Text) - 1);
                    
                }
            
            }
            if (listView1.Items.Count == 0)
            {
                //btnSend.Enabled = false;
                //btnDelete.Enabled = false;
            }


        }

        private void txtBarcode_LostFocus(object sender, EventArgs e)
        {
            this.btnGoBarCode.Enabled = true;
            this.btnGoBarCode.BackColor = Color.Khaki;
        }

        private void txtBarcode_GotFocus(object sender, EventArgs e)
        {
            btnGoBarCode.Enabled = false;
            btnGoBarCode.BackColor = Color.Gainsboro;
        }

        private void menuItem7_Click(object sender, EventArgs e)
        {
         
        }

        private void mnu1Unit_Click(object sender, EventArgs e)
        {
          
        }

        private void menuItem7_Click_1(object sender, EventArgs e)
        {
   mnu1Unit.Checked = true;
   mnu2Unit.Checked = false;
   mnu3Unit.Checked = false;
        }

        private void mnu2Unit_Click(object sender, EventArgs e)
        {
            mnu1Unit.Checked = false;
            mnu2Unit.Checked = false;
            mnu3Unit.Checked = true;
        }

        private void mnu2Unit_Click_1(object sender, EventArgs e)
        {
            mnu1Unit.Checked = false;
            mnu2Unit.Checked = true;
            mnu3Unit.Checked = false;
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
           
                if (this.listView1.Items.Count > 0)
                { if (MessageBox.Show("Do you want to clear display?", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
            {
                    this.listView1.Items.Clear();
                    this.lblCount.Text = "0";
                }
            }
        }
        
        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (this.listView1.SelectedIndices.Count > 0)
            {
                OldBarcode = this.listView1.FocusedItem.SubItems[0].Text;
                 frmEdit frm = new frmEdit();
                 if (frm.ShowDialog() == System.Windows.Forms.DialogResult.OK) {
                     this.listView1.FocusedItem.SubItems[0].Text = SocketClient.frmEdit.NewBarcode;
                }
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            frmAdd frm = new frmAdd();
            if (frm.ShowDialog() == DialogResult.OK)
            {
                string temp = SocketClient.frmAdd.Barcode.Substring(0, SocketClient.frmAdd.Barcode.Length - 3);
                int running = Convert.ToInt32(SocketClient.frmAdd.Barcode.Substring(SocketClient.frmAdd.Barcode.Length - 3, 3));
                int leng = Convert.ToInt32(SocketClient.frmAdd.Running);
            
                for (int i = 1; i <= leng; i++)
                {
                    if (this.listView1.Items.Count < 100)
                    {
                        ListViewItem lvw = new ListViewItem();
                        lvw.SubItems[0].Text = temp + running.ToString("000");
                        listView1.Items.Add(lvw);
                        lvw = null;
                        lblCount.Text = (Convert.ToInt32(lblCount.Text) + 1).ToString();
                    }
                    else
                    {
                        MessageBox.Show("Maximum Line is 100 Line");
                        return;
                    }
                  running += 1;
                  
                }
            
            }
        }

        private void menuItem5_Click(object sender, EventArgs e)
        {
            //OpenFileDialog dlg = new OpenFileDialog();
            // ////string TextFile = "Text Files (*.txt)|*.txt";
            // //dlg.Filter = TextFile; //+ allPicture; //allPicture +  "|" + jpeg + "|" + gif + "|" + png + "|" + bitmap;
             //string path = "\\Program Files\\socketclient\\Import Data\\Barcode.txt";
            //OpenFileDialogEx dlg = new OpenFileDialogEx();
            //dlg.Filter = "*.*";
             WindowsMobile.CommonDialogs.OpenFileDialog dlg = new WindowsMobile.CommonDialogs.OpenFileDialog();
             dlg.Filter = "Text File|*.txt";
            if (dlg.ShowDialog() == DialogResult.OK) 
                {
                    if (File.Exists(dlg.FileName) == false) 
                      {
                          MessageBox.Show("Cannot find File .txt !");
                          return;
                      }
                    using (StreamReader reader = new StreamReader(dlg.FileName))
                      {
                              string line = "";
                              while (line != null) {
                              line = reader.ReadLine();
                              if (string.IsNullOrEmpty(line) || line == null)
                              {
                                  continue;
                              }
                              else
                              {
                                  if (this.listView1.Items.Count < 100)
                                  {
                                      ListViewItem lvw = new ListViewItem();
                                      lvw.SubItems[0].Text = line;
                                      listView1.Items.Add(lvw);
                                      lvw = null;
                                      lblCount.Text = (Convert.ToInt32(lblCount.Text) + 1).ToString();
                                  }
                                  else
                                  {
                                      MessageBox.Show("Maximum Line is 100 Line");
                                  }
                                  
                              }

                        }
                      }
                }

            dlg.Dispose();

        }

        private void MyReader_ReadNotify(object sender, EventArgs e)
        {
            
            Symbol.Barcode.ReaderData TheReaderData = Scanning.MyReaderData;
            //if (TheReaderData.Result == Symbol.Results.CANCELED) {
            //    Scanning.StopRead();
            //    Scanning.StartRead();
            //    return;
            //}

                if (mnuMode1.Checked || mnuMode2.Checked)
                {
                    if (this.listView1.Visible == true)
                    {
                        string barcode = "";
                        if (TheReaderData.Result == Symbol.Results.SUCCESS)
                        {
                            barcode = TheReaderData.Text;
                            Scanning.StartRead();
                        }
                        if (this.listView1.Items.Count < 100)
                        {
                            //if (isActive == true)
                            //{
                            ListViewItem lvw = new ListViewItem();
                            lvw.SubItems[0].Text = barcode;
                            listView1.Items.Add(lvw);
                            lblCount.Text = Convert.ToString(Convert.ToInt32(lblCount.Text) + 1);

                            //}
                            //else
                            //{

                            //SendKeys.Send(barcode+"/n");
                              
                                //messageQueue1.Close();
                            //}
                        }
                        else
                        {
                            // Terminate reader
                            Scanning.TermReader();
                            MessageBox.Show("Maximum Line is 100 Line");
                            //this.MyReader.Actions.CancelRead(this.MyReaderData);
                            //Edit 9/05/13

                            //return;

                            ///////
                            if (Scanning.InitReader())
                            {
                                // Create a new delegate to handle scan notifications
                                ReaderFormEventHandler = new EventHandler(MyReader_ReadNotify);

                                // Set the event handler of the Scanning class to our delegate
                                Scanning.MyEventHandler = ReaderFormEventHandler;

                                // Attach to activate and deactivate events
                                this.Activated += new EventHandler(ReaderForm_Activated);

                                // Start a read on the reader
                                Scanning.StartRead();
                            }
                            else
                            {
                                // If not, close this form
                                txtBarcode.Text = "";
                                txtBarcode.Focus();
                                btnDelete.Enabled = true;
                                btnSend.Enabled = true;
                                //this.Close();

                                return;
                            }
                        }

                        txtBarcode.Text = "";
                        txtBarcode.Focus();
                        btnDelete.Enabled = true;
                        btnSend.Enabled = true;
                    }

                }
            
        }

        //private void MyRadio_StatusNotify(object sender, EventArgs e)
        //{
        //    Symbol.WirelessLAN.WLANStatus MyStatus = this.MyRadio.GetNextStatus();

        //    if (MyStatus != null)
        //    {
        //        if (mnuMode1.Checked == true)
        //        { 
        //         switch (MyStatus.Change)
        //         {
        //             case Symbol.WirelessLAN.ChangeType.SIGNAL:
        //                 this.chkSignal(this.MyRadio.Signal);
        //                 break;
        //         }
                
                
                
        //        }
              
        //    }
        //}

        //private void chkSignal(Symbol.WirelessLAN.Signal SignalInfo)
        //{
        //    //if (pWName != "")
        //    //{
        //    //    pnSignal.Visible = false;

        //    //}
        //    //else
        //    //{
        //    //    pnSignal.Visible = true;
        //    //}
        //    this.ProgressBarSignal.Value = SignalInfo.Percent;
        //    if (Convert.ToInt32(SignalInfo.Percent.ToString()) <= 20)
        //    {
        //        this.LabelSignal.ForeColor = Color.Red;
        //        this.LabelSignalPercent.ForeColor = Color.Red;
        //    }
        //    else {
        //        this.LabelSignal.ForeColor = Color.Green;
        //        this.LabelSignalPercent.ForeColor = Color.Green;
        //    }

        //    this.LabelSignalPercent.Text = SignalInfo.Percent.ToString() + "%";
        //    this.LabelSignal.Text = SignalInfo.Text;
        
        //}

        private void frmMain_Closing(object sender, CancelEventArgs e)
        {
            Scanning.TermReader();
            myWlan.Adapters[0].SignalQualityChanged -= mySignalQualityHandler;
            deleteSampleProfiles();
            myWlan.Dispose();
            myConfig.Dispose();
        }

        private void frmMain_Deactivate(object sender, EventArgs e)
        {
          
        }
        void myAdapter_SignalQualityChanged(object sender, StatusChangeArgs e)
        {
            //populate signal quality and strength in the list

            displaySignal(e.SignalQuality.ToString());

        }
        private void displaySignal(string pSignal) {
            if (pSignal == "EXCELLENT")
            {
                this.lblShowSignal.Text = "Excellent 100% ";
                this.ProgressBarSignal.Value = 100;
                this.lblShowSignal.ForeColor = Color.Green;
                
            }
            else if(pSignal=="VERYGOOD"){
                this.lblShowSignal.Text = "Very Good 80% ";
                this.ProgressBarSignal.Value = 80;
                this.lblShowSignal.ForeColor = Color.Green;
            }
            else if (pSignal == "GOOD") {
                this.lblShowSignal.Text = "Good 60% ";
                this.ProgressBarSignal.Value = 60;
                this.lblShowSignal.ForeColor = Color.Gold;
            }
            else if (pSignal == "FAIR") {
                this.lblShowSignal.Text = "Fair 40% ";
                this.ProgressBarSignal.Value = 40;
                this.lblShowSignal.ForeColor = Color.Gold;
            
            }
            else if (pSignal == "POOR") {
                this.lblShowSignal.Text = "Poor 20% ";
                this.ProgressBarSignal.Value = 20;
                this.lblShowSignal.ForeColor = Color.Red;
            }
            else if (pSignal == "NONE")
            {
                this.lblShowSignal.Text = "Bad 0%";
                this.ProgressBarSignal.Value = 0;
                this.lblShowSignal.ForeColor = Color.Red;
            }
            else {
                this.lblShowSignal.Text = "Please Wait !!!";
                this.ProgressBarSignal.Value = 0;
                this.lblShowSignal.ForeColor = Color.Red;
            }
        
        }
        
      
        private void deleteSampleProfiles()
        {
            //Create a WLAN object in COMMAND_MODE
            WLAN myCommandModeWlan = null;
            try
            {
                myCommandModeWlan = new WLAN(FusionAccessType.COMMAND_MODE);
            }
            catch (OperationFailureException)
            {
                System.Windows.Forms.MessageBox.Show("Command mode is in use", "CS_FusionSample1");

                return;
            }

            //Delete the profile SampleAdhoc only if it has been created by this sample itself. 
            if (bAdhocCreated)
            {
                //get a reference to the SampleAdhoc profile
                myAdhocProfile = getProfileByName("SampleAdhoc", myCommandModeWlan);
                if (myAdhocProfile != null)
                {
                    myCommandModeWlan.Profiles.DeleteProfile(myAdhocProfile);
                    bAdhocCreated = false;
                }
            }

            //Delete the profile SampleInfrastructure only if it has been created by this sample itself. 
            if (bInfraCreated)
            {
                //get a reference to the SampleInfrastructure profile
                myInfrastructureProfile = getProfileByName("SampleInfrastructure", myCommandModeWlan);
                if (myInfrastructureProfile != null)
                {
                    myCommandModeWlan.Profiles.DeleteProfile(myInfrastructureProfile);
                    bInfraCreated = false;
                }
            }

            //dispose the WLAN object
            myCommandModeWlan.Dispose();
            myCommandModeWlan = null;

        }
      
    }
}