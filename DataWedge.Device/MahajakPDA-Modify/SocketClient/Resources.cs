//--------------------------------------------------------------------
// FILENAME: Resources.cs
//
// Copyright(c) 2007 Motorola Inc. All rights reserved.
//
// DESCRIPTION:
//
// NOTES:
//
// 
//--------------------------------------------------------------------

using System.Globalization;
using System.Resources;


namespace SocketClient
{
	internal class Resources
	{
		static System.Resources.ResourceManager m_rmNameValues;

		static Resources()
		{
			m_rmNameValues= new System.Resources.ResourceManager(
				"FusionSample1.Resources", typeof(Resources).Assembly);
		}

		public static string GetString(string name)
		{
			return m_rmNameValues.GetString(name);
		}
	}
}
