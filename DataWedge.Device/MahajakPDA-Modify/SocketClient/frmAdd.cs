﻿using System;

using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;


namespace SocketClient
{
    public partial class frmAdd : Form
    {
        private bool chkNo = false;
        public static string Barcode = "";
        public static string Running = "";
        private System.EventHandler ReaderFormEventHandler;
        public frmAdd()
        {
            InitializeComponent();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            chkNo = true;
            this.DialogResult = DialogResult.No;
        }
        private void clearData()
        {
            this.txtBarcode.Text = "";
            if (cboRuning.Items.Count > 1)
                    cboRuning.Items.Clear();
            for (int i = 1 ; i<=100;i++)
            {
                
                cboRuning.Items.Add(i);
            }
            cboRuning.SelectedIndex = 0;
        
        }

        private void frmAdd_Load(object sender, EventArgs e)
        {
            clearData();
            chkNo = false;
            Scanning.TermReader();
            if (Scanning.InitReader())
            {
                // Create a new delegate to handle scan notifications
                ReaderFormEventHandler = new EventHandler(MyReader_ReadNotify);

                // Set the event handler of the Scanning class to our delegate
                Scanning.MyEventHandler = ReaderFormEventHandler;

                // Attach to activate and deactivate events
                this.Activated += new EventHandler(ReaderForm_Activated);

                // Start a read on the reader
                Scanning.StartRead();
            }
            else
            {
                // If not, close this form
           

                return;
            }
        }
        private void MyReader_ReadNotify(object sender, EventArgs e)
        {
            Symbol.Barcode.ReaderData TheReaderData = Scanning.MyReaderData;
            if (TheReaderData.Result == Symbol.Results.SUCCESS)
            {
                this.txtBarcode.Text = TheReaderData.Text;
            }
        
        }
        private void ReaderForm_Activated(object sender, EventArgs e)
        {
            // Reset the scan event handler for the Scanning object to the form's delegate. 
            Scanning.MyEventHandler = ReaderFormEventHandler;

            // Start the next read
            Scanning.StartRead();
        }
        public Boolean IsNumeric(string num)
        {

            try
            {

                int newNum = Int32.Parse(num);

                return true;

            }

            catch
            {

                return false;

            }



        }

        private void btnOK_Click(object sender, EventArgs e)
        {
                      if (this.txtBarcode.Text.Length < 3)
                {
                    MessageBox.Show("Length of Barcode must > 2");
                    return;
                
                
                }
            if (IsNumeric(txtBarcode.Text.Substring(txtBarcode.Text.Length - 3, 3)) == false)
                {
                    MessageBox.Show("Farmat Barcode is wrong");
                    return;

                }
          
            chkNo = false;
            Barcode = txtBarcode.Text;
            Running = cboRuning.Text;
            this.DialogResult = DialogResult.OK;
        }

        private void frmAdd_KeyDown(object sender, KeyEventArgs e)
        {
            if ((e.KeyCode == System.Windows.Forms.Keys.Up))
            {
                // Up
            }
            if ((e.KeyCode == System.Windows.Forms.Keys.Down))
            {
                // Down
            }
            if ((e.KeyCode == System.Windows.Forms.Keys.Left))
            {
                // Left
            }
            if ((e.KeyCode == System.Windows.Forms.Keys.Right))
            {
                // Right
            }
            if ((e.KeyCode == System.Windows.Forms.Keys.Enter))
            {
                // Enter
            }

        }

        private void frmAdd_Closing(object sender, CancelEventArgs e)
        {
            if (chkNo == true)
            {
                Scanning.TermReader();
                this.DialogResult = DialogResult.No;
            }
            else
            {

                if (this.txtBarcode.Text.Length < 3)
                {
                    MessageBox.Show("Length of Barcode must > 2");
                    e.Cancel = true;
                    return;


                }
                if (IsNumeric(txtBarcode.Text.Substring(txtBarcode.Text.Length - 3, 3)) == false)
                {
                    MessageBox.Show("Farmat Barcode is wrong");
                    e.Cancel = true;
                    return;

                }

                chkNo = false;
                Barcode = txtBarcode.Text;
                Running = cboRuning.Text;
                Scanning.TermReader();
                this.DialogResult = DialogResult.OK;

            }
        }

    }
}