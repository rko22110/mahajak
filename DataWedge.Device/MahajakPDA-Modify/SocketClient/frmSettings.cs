﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using OpenNETCF.Configuration;
using System.IO;

namespace SocketClient
{
    public partial class frmSettings : Form
    {
        private String IPAddress = "";
        private String Port = "";
        //private ConfigSettings config = new ConfigSettings();
        
        public frmSettings()
        {
            InitializeComponent();
        }

        private void frmSettings_Load(object sender, EventArgs e)
        {
            //config.SectionName = "appSettings";

            //txtIP.Text = config.GetValue("ServerIP");
            //txtPort.Text = config.GetValue("ServerPort");
            Scanning.TermReader();
            getbase();
            txtIP.Text = IPAddress;//ConfigurationSettings.AppSettings["ServerIP"];
            txtPort.Text = Port;//ConfigurationSettings.AppSettings["ServerPort"];
        }
     
        private void getbase()
        {
            //string fileName = "./config.ini";
            //string path = Application.StartupPath + "//" + fileName;
            int rec = 0;
            string line = "";
            StreamReader sr = new StreamReader(Path.GetDirectoryName( System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase)+"\\config.ini");
            do
            {
                if (rec == 0)
                {
                    line = sr.ReadLine();
                  IPAddress = line.Remove(0, 11);
                }
            
                if (rec == 1)
                {
                    line = sr.ReadLine();
                    Port = line.Remove(0, 6);
                    break;
                }
                rec += 1;
                sr.Peek();
            } while (rec <= 3);
            sr.Close();
           
        }
        private void frmSettings_Closed(object sender, EventArgs e)
        {


            string path = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase) + "\\config.ini";
            //StringBuilder classBudlder = new StringBuilder();
            bool chk = System.IO.File.Exists(path);

            if (chk == true)
            {
                System.IO.File.Delete(path);

            }
            StreamWriter writer = System.IO.File.CreateText(path);
            writer.WriteLine("<IPAddress>" + txtIP.Text);
            writer.WriteLine("<Port>" + txtPort.Text);
            writer.Close();
            DialogResult = DialogResult.OK;
        }

        //private void button1_Click(object sender, EventArgs e)
        //{
        //    ConfigurationSettings.AppSettings.Set("ServerIP", txtIP.Text);
        //    //OpenNETCF.Configuration.ConfigurationSettings.AppSettings.Set("ServerIP", txtIP.Text);
        //   //ConfigurationSettings.AppSettings["ServerIP"] = txtIP.Text;
          
        //}
    }
}