﻿using System;

using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace SocketClient
{
    public partial class frmEdit : Form
    {
        private bool chkNo = false;
        private System.EventHandler ReaderFormEventHandler;
        public static string NewBarcode = "";
        public frmEdit()
        {
            InitializeComponent();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
           chkNo = true;
          this.DialogResult = DialogResult.No;
        }

        private void txtBarcode_KeyPress(object sender, KeyPressEventArgs e)
        {
            
        }

        private void frmEdit_Load(object sender, EventArgs e)
        {
            this.txtBarcode.Text = SocketClient.frmMain.OldBarcode;
            chkNo = false;
            Scanning.TermReader();
            if (Scanning.InitReader())
            {
                // Create a new delegate to handle scan notifications
                ReaderFormEventHandler = new EventHandler(MyReader_ReadNotify);

                // Set the event handler of the Scanning class to our delegate
                Scanning.MyEventHandler = ReaderFormEventHandler;

                // Attach to activate and deactivate events
                this.Activated += new EventHandler(ReaderForm_Activated);

                // Start a read on the reader
                Scanning.StartRead();
            }
            else
            {
                // If not, close this form


                return;
            }

        }
        private void MyReader_ReadNotify(object sender, EventArgs e)
        {
            Symbol.Barcode.ReaderData TheReaderData = Scanning.MyReaderData;
            if (TheReaderData.Result == Symbol.Results.SUCCESS)
            {
                this.txtBarcode.Text = TheReaderData.Text;
            }

        }
        private void ReaderForm_Activated(object sender, EventArgs e)
        {
            // Reset the scan event handler for the Scanning object to the form's delegate. 
            Scanning.MyEventHandler = ReaderFormEventHandler;

            // Start the next read
            Scanning.StartRead();
        }
        private void btnOK_Click(object sender, EventArgs e)
        {
            chkNo = false;
            if (this.txtBarcode.Text == "")
            {
                MessageBox.Show("Barcode can't blank !");

            }
            else
            {
                NewBarcode = txtBarcode.Text;
                this.DialogResult = DialogResult.OK;
            }
        }

        private void frmEdit_Closing(object sender, CancelEventArgs e)
        {
            if (chkNo == true)
            {
                Scanning.TermReader();
                this.DialogResult = DialogResult.No;
            }
            else
            {
                if (this.txtBarcode.Text == "")
                {
                    MessageBox.Show("Barcode can't blank !");
                    e.Cancel = true;
                }
                else
                {
                    NewBarcode = txtBarcode.Text;
                    Scanning.TermReader();
                    this.DialogResult = DialogResult.OK;
                }
            }
        }
    }
}