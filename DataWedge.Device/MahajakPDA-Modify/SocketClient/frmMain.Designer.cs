﻿using Symbol.Fusion.WLAN;
using Symbol.Fusion;
namespace SocketClient
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label4 = new System.Windows.Forms.Label();
            this.txtBarcode = new System.Windows.Forms.TextBox();
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.menuItem1 = new System.Windows.Forms.MenuItem();
            this.mnuConnect = new System.Windows.Forms.MenuItem();
            this.mnuDisconnect = new System.Windows.Forms.MenuItem();
            this.menuItem4 = new System.Windows.Forms.MenuItem();
            this.mnuSettings = new System.Windows.Forms.MenuItem();
            this.menuItem2 = new System.Windows.Forms.MenuItem();
            this.menuItem3 = new System.Windows.Forms.MenuItem();
            this.mnuMenu = new System.Windows.Forms.MenuItem();
            this.mnuMode1 = new System.Windows.Forms.MenuItem();
            this.mnuMode2 = new System.Windows.Forms.MenuItem();
            this.menuItem6 = new System.Windows.Forms.MenuItem();
            this.mnu1Unit = new System.Windows.Forms.MenuItem();
            this.mnu2Unit = new System.Windows.Forms.MenuItem();
            this.mnu3Unit = new System.Windows.Forms.MenuItem();
            this.mnuBrower = new System.Windows.Forms.MenuItem();
            this.btnSend = new System.Windows.Forms.Button();
            this.serialPort1 = new System.IO.Ports.SerialPort(this.components);
            this.lblMode = new System.Windows.Forms.Label();
            this.listView1 = new System.Windows.Forms.ListView();
            this.colBarCode = new System.Windows.Forms.ColumnHeader();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnGoBarCode = new System.Windows.Forms.Button();
            this.pnShowConnect = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lblBarcode = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lblCount = new System.Windows.Forms.Label();
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnEdit = new System.Windows.Forms.Button();
            this.btnClear = new System.Windows.Forms.Button();
            this.pnSignal = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.ProgressBarSignal = new System.Windows.Forms.ProgressBar();
            this.lblShowSignal = new System.Windows.Forms.Label();
            this.pnShowConnect.SuspendLayout();
            this.pnSignal.SuspendLayout();
            this.SuspendLayout();
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.Color.Black;
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(12, 29);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(113, 16);
            this.label4.Text = "Barcode Data";
            // 
            // txtBarcode
            // 
            this.txtBarcode.Location = new System.Drawing.Point(48, 279);
            this.txtBarcode.Name = "txtBarcode";
            this.txtBarcode.Size = new System.Drawing.Size(63, 21);
            this.txtBarcode.TabIndex = 39;
            this.txtBarcode.GotFocus += new System.EventHandler(this.txtBarcode_GotFocus);
            this.txtBarcode.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textMsg_KeyPress);
            this.txtBarcode.LostFocus += new System.EventHandler(this.txtBarcode_LostFocus);
            // 
            // mainMenu1
            // 
            this.mainMenu1.MenuItems.Add(this.menuItem1);
            this.mainMenu1.MenuItems.Add(this.mnuMenu);
            // 
            // menuItem1
            // 
            this.menuItem1.MenuItems.Add(this.mnuConnect);
            this.menuItem1.MenuItems.Add(this.mnuDisconnect);
            this.menuItem1.MenuItems.Add(this.menuItem4);
            this.menuItem1.MenuItems.Add(this.mnuSettings);
            this.menuItem1.MenuItems.Add(this.menuItem2);
            this.menuItem1.MenuItems.Add(this.menuItem3);
            this.menuItem1.Text = "Menu";
            // 
            // mnuConnect
            // 
            this.mnuConnect.Text = "Connect";
            this.mnuConnect.Click += new System.EventHandler(this.btnConnect_Click);
            // 
            // mnuDisconnect
            // 
            this.mnuDisconnect.Enabled = false;
            this.mnuDisconnect.Text = "Disconnect";
            this.mnuDisconnect.Click += new System.EventHandler(this.btnDisconnect_Click);
            // 
            // menuItem4
            // 
            this.menuItem4.Text = "-";
            // 
            // mnuSettings
            // 
            this.mnuSettings.Text = "Settings";
            this.mnuSettings.Click += new System.EventHandler(this.mnuSettings_Click);
            // 
            // menuItem2
            // 
            this.menuItem2.Text = "-";
            // 
            // menuItem3
            // 
            this.menuItem3.Text = "Exit";
            this.menuItem3.Click += new System.EventHandler(this.menuItem3_Click);
            // 
            // mnuMenu
            // 
            this.mnuMenu.MenuItems.Add(this.mnuMode1);
            this.mnuMenu.MenuItems.Add(this.mnuMode2);
            this.mnuMenu.MenuItems.Add(this.menuItem6);
            this.mnuMenu.MenuItems.Add(this.mnuBrower);
            this.mnuMenu.Text = "Mode";
            // 
            // mnuMode1
            // 
            this.mnuMode1.Checked = true;
            this.mnuMode1.Text = "DataWedge";
            this.mnuMode1.Click += new System.EventHandler(this.mnuMode1_Click);
            // 
            // mnuMode2
            // 
            this.mnuMode2.Text = "Print Barcode";
            this.mnuMode2.Click += new System.EventHandler(this.mnuMode2_Click);
            // 
            // menuItem6
            // 
            this.menuItem6.MenuItems.Add(this.mnu1Unit);
            this.menuItem6.MenuItems.Add(this.mnu2Unit);
            this.menuItem6.MenuItems.Add(this.mnu3Unit);
            this.menuItem6.Text = "Print Option";
            // 
            // mnu1Unit
            // 
            this.mnu1Unit.Text = "1 Unit";
            this.mnu1Unit.Click += new System.EventHandler(this.menuItem7_Click_1);
            // 
            // mnu2Unit
            // 
            this.mnu2Unit.Text = "2 Unit";
            this.mnu2Unit.Click += new System.EventHandler(this.mnu2Unit_Click_1);
            // 
            // mnu3Unit
            // 
            this.mnu3Unit.Checked = true;
            this.mnu3Unit.Text = "3 Unit";
            this.mnu3Unit.Click += new System.EventHandler(this.mnu2Unit_Click);
            // 
            // mnuBrower
            // 
            this.mnuBrower.Text = "Brower Data (.txt)";
            this.mnuBrower.Click += new System.EventHandler(this.menuItem5_Click);
            // 
            // btnSend
            // 
            this.btnSend.BackColor = System.Drawing.Color.PaleGreen;
            this.btnSend.ForeColor = System.Drawing.Color.Black;
            this.btnSend.Location = new System.Drawing.Point(156, 216);
            this.btnSend.Name = "btnSend";
            this.btnSend.Size = new System.Drawing.Size(70, 26);
            this.btnSend.TabIndex = 41;
            this.btnSend.Text = "Send Data";
            this.btnSend.Click += new System.EventHandler(this.btnSend_Click);
            // 
            // lblMode
            // 
            this.lblMode.BackColor = System.Drawing.Color.Black;
            this.lblMode.ForeColor = System.Drawing.Color.White;
            this.lblMode.Location = new System.Drawing.Point(12, 7);
            this.lblMode.Name = "lblMode";
            this.lblMode.Size = new System.Drawing.Size(214, 16);
            this.lblMode.Text = "Mode: DataWedge";
            // 
            // listView1
            // 
            this.listView1.Columns.Add(this.colBarCode);
            this.listView1.FullRowSelect = true;
            this.listView1.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.listView1.Location = new System.Drawing.Point(12, 48);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(214, 138);
            this.listView1.TabIndex = 0;
            this.listView1.View = System.Windows.Forms.View.Details;
            // 
            // colBarCode
            // 
            this.colBarCode.Text = "Barcode";
            this.colBarCode.Width = 211;
            // 
            // btnDelete
            // 
            this.btnDelete.BackColor = System.Drawing.Color.Red;
            this.btnDelete.Location = new System.Drawing.Point(156, 188);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(70, 26);
            this.btnDelete.TabIndex = 45;
            this.btnDelete.Text = "Delete";
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnGoBarCode
            // 
            this.btnGoBarCode.BackColor = System.Drawing.Color.Khaki;
            this.btnGoBarCode.Location = new System.Drawing.Point(12, 188);
            this.btnGoBarCode.Name = "btnGoBarCode";
            this.btnGoBarCode.Size = new System.Drawing.Size(63, 26);
            this.btnGoBarCode.TabIndex = 45;
            this.btnGoBarCode.Text = "GO";
            this.btnGoBarCode.Click += new System.EventHandler(this.btnGoBarCode_Click);
            // 
            // pnShowConnect
            // 
            this.pnShowConnect.BackColor = System.Drawing.Color.MistyRose;
            this.pnShowConnect.Controls.Add(this.label2);
            this.pnShowConnect.Controls.Add(this.label1);
            this.pnShowConnect.Location = new System.Drawing.Point(12, 48);
            this.pnShowConnect.Name = "pnShowConnect";
            this.pnShowConnect.Size = new System.Drawing.Size(214, 194);
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Tahoma", 18F, System.Drawing.FontStyle.Bold);
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(14, 99);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(191, 39);
            this.label2.Text = "Pda To Server.";
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Tahoma", 18F, System.Drawing.FontStyle.Bold);
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(6, 58);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(206, 39);
            this.label1.Text = "Please Connect";
            // 
            // lblBarcode
            // 
            this.lblBarcode.BackColor = System.Drawing.Color.White;
            this.lblBarcode.Font = new System.Drawing.Font("Tahoma", 18F, System.Drawing.FontStyle.Bold);
            this.lblBarcode.ForeColor = System.Drawing.Color.Black;
            this.lblBarcode.Location = new System.Drawing.Point(200, -2);
            this.lblBarcode.Name = "lblBarcode";
            this.lblBarcode.Size = new System.Drawing.Size(37, 19);
            this.lblBarcode.Visible = false;
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.Black;
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(131, 29);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(48, 16);
            this.label3.Text = "Count";
            // 
            // lblCount
            // 
            this.lblCount.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.lblCount.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.lblCount.ForeColor = System.Drawing.Color.Red;
            this.lblCount.Location = new System.Drawing.Point(178, 29);
            this.lblCount.Name = "lblCount";
            this.lblCount.Size = new System.Drawing.Size(48, 16);
            this.lblCount.Text = "0";
            this.lblCount.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // btnAdd
            // 
            this.btnAdd.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btnAdd.ForeColor = System.Drawing.Color.Black;
            this.btnAdd.Location = new System.Drawing.Point(80, 188);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(71, 26);
            this.btnAdd.TabIndex = 41;
            this.btnAdd.Text = "Add";
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnEdit
            // 
            this.btnEdit.BackColor = System.Drawing.Color.Chocolate;
            this.btnEdit.ForeColor = System.Drawing.Color.Black;
            this.btnEdit.Location = new System.Drawing.Point(12, 216);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(63, 26);
            this.btnEdit.TabIndex = 41;
            this.btnEdit.Text = "Edit";
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // btnClear
            // 
            this.btnClear.BackColor = System.Drawing.Color.DarkOrchid;
            this.btnClear.ForeColor = System.Drawing.Color.Black;
            this.btnClear.Location = new System.Drawing.Point(80, 216);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(71, 26);
            this.btnClear.TabIndex = 41;
            this.btnClear.Text = "Clear";
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // pnSignal
            // 
            this.pnSignal.BackColor = System.Drawing.Color.Brown;
            this.pnSignal.Controls.Add(this.label7);
            this.pnSignal.Controls.Add(this.label6);
            this.pnSignal.Controls.Add(this.label5);
            this.pnSignal.Location = new System.Drawing.Point(12, 3);
            this.pnSignal.Name = "pnSignal";
            this.pnSignal.Size = new System.Drawing.Size(214, 239);
            this.pnSignal.Visible = false;
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("Tahoma", 30F, System.Drawing.FontStyle.Regular);
            this.label7.ForeColor = System.Drawing.Color.Blue;
            this.label7.Location = new System.Drawing.Point(19, 165);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(184, 59);
            this.label7.Text = "Signal !!!";
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Tahoma", 30F, System.Drawing.FontStyle.Regular);
            this.label6.ForeColor = System.Drawing.Color.Blue;
            this.label6.Location = new System.Drawing.Point(33, 85);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(153, 59);
            this.label6.Text = "Wireless";
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Tahoma", 30F, System.Drawing.FontStyle.Regular);
            this.label5.ForeColor = System.Drawing.Color.Blue;
            this.label5.Location = new System.Drawing.Point(18, 4);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(181, 57);
            this.label5.Text = "Not have";
            // 
            // ProgressBarSignal
            // 
            this.ProgressBarSignal.Location = new System.Drawing.Point(104, 248);
            this.ProgressBarSignal.Name = "ProgressBarSignal";
            this.ProgressBarSignal.Size = new System.Drawing.Size(125, 12);
            // 
            // lblShowSignal
            // 
            this.lblShowSignal.Location = new System.Drawing.Point(10, 246);
            this.lblShowSignal.Name = "lblShowSignal";
            this.lblShowSignal.Size = new System.Drawing.Size(92, 20);
            this.lblShowSignal.Text = "Please Wait !!!";
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(240, 268);
            this.ControlBox = false;
            this.Controls.Add(this.ProgressBarSignal);
            this.Controls.Add(this.pnSignal);
            this.Controls.Add(this.pnShowConnect);
            this.Controls.Add(this.btnGoBarCode);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.listView1);
            this.Controls.Add(this.lblMode);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.btnEdit);
            this.Controls.Add(this.btnSend);
            this.Controls.Add(this.lblBarcode);
            this.Controls.Add(this.txtBarcode);
            this.Controls.Add(this.lblCount);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.lblShowSignal);
            this.Menu = this.mainMenu1;
            this.MinimizeBox = false;
            this.Name = "frmMain";
            this.Text = "DataWedge";
            this.Deactivate += new System.EventHandler(this.frmMain_Deactivate);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Closed += new System.EventHandler(this.frmMain_Closed);
            this.Activated += new System.EventHandler(this.frmMain_Activated);
            this.GotFocus += new System.EventHandler(this.frmMain_GotFocus);
            this.Closing += new System.ComponentModel.CancelEventHandler(this.frmMain_Closing);
            this.pnShowConnect.ResumeLayout(false);
            this.pnSignal.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtBarcode;
        private System.Windows.Forms.MainMenu mainMenu1;
        private System.Windows.Forms.MenuItem menuItem1;
        private System.Windows.Forms.MenuItem mnuConnect;
        private System.Windows.Forms.MenuItem mnuDisconnect;
        private System.Windows.Forms.MenuItem menuItem4;
        private System.Windows.Forms.MenuItem mnuSettings;
        private System.Windows.Forms.MenuItem menuItem2;
        private System.Windows.Forms.MenuItem menuItem3;
        private System.Windows.Forms.Button btnSend;
        private System.Windows.Forms.MenuItem mnuMenu;
        private System.Windows.Forms.MenuItem mnuMode1;
        private System.Windows.Forms.MenuItem mnuMode2;
        private System.IO.Ports.SerialPort serialPort1;
        private System.Windows.Forms.Label lblMode;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.ColumnHeader colBarCode;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnGoBarCode;
        private System.Windows.Forms.Panel pnShowConnect;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblBarcode;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblCount;
        private System.Windows.Forms.MenuItem menuItem6;
        private System.Windows.Forms.MenuItem mnu1Unit;
        private System.Windows.Forms.MenuItem mnu3Unit;
        private System.Windows.Forms.MenuItem mnu2Unit;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnEdit;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.MenuItem mnuBrower;
        //private Barcode.Barcode barcode1;
        private System.Windows.Forms.Panel pnSignal;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ProgressBar ProgressBarSignal;
        private System.Windows.Forms.Label lblShowSignal;
    }
}

